#!/usr/bin/env python3.11
"""
Utilities for AIO's parameters: upload/download, search, compare, repair

SEE: tools/aio-param-conversion first

OBS documentation:
Upload AIO settings.
Input file is a tab-delimited file:
    feed-key    p1      p2      p3...
Where feed-key does not have the "p6.p6-" like prefix.
A blank feed-key marks a line to be skipped.

Try: copy cells from spreadsheet, paste into a file.
"""

MaxAIORate = 30 # per minute

import argparse,csv
import subprocess,json,re,asyncio
from datetime import datetime,timedelta
from functools import reduce

import sys,os
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from simple_debug import debug
from aio_key import aio_key
from recipe_dsl import h_duration

import asyncio_mqtt as mqtt
import paho.mqtt as paho # some constants|--help'

AIOJSONDir = 'aio-json'
FeedInfoFile = f'{AIOJSONDir}/aio-feed-info.json'
AIOJSONFile = '.aio.json'

mqtt_config = {
            "hostname" : "io.adafruit.com",
            "port" : 8883, # should ssl?
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
            ## Filled in later:
            #"username" : os.getenv('IO_USERNAME',None),
            #"password" : os.getenv('IO_KEY',None),
            "tls_params" : mqtt.TLSParameters(),
}

def fail(msg, status=1):
    sys.stderr.write(msg)
    sys.stderr.write("\n")
    exit(status)

async def do_get_feed_info(parsedargs):
    subprocess.run(
    ['curl', '-o',  FeedInfoFile, 
    f"https://io.adafruit.com/api/v2/{aio_key()['IO_USERNAME']}/feeds?x-aio-key={aio_key()['IO_KEY']}"
    ])
    print(f"# Fetched {FeedInfoFile}")

def _feed_info(feedinfo_file = FeedInfoFile):
    # cache all 3 things: key->value, full feed-info, group-keys
    # return them all
    # use the specialized cache'rs for each
    global _feedinfo,_feedinfo_kv, _feedinfo_groups
    if not globals().get('_feedinfo'):
        with open(feedinfo_file, 'r') as fh:
            _feedinfo = json.load(fh)

        _feedinfo_groups = {}
        _feedinfo_kv = {}

        for afeed in _feedinfo:
            this_key = afeed['key']

            # k:v
            _feedinfo_kv[ afeed['key'] ] = afeed['last_value']

            # identify groups
            for agroup in afeed.get('groups',[]):
                if not _feedinfo_groups.get( agroup['key'], None):
                    _feedinfo_groups[ agroup['key'] ] = []
                _feedinfo_groups[ agroup['key'] ].append( this_key )

    return (_feedinfo, _feedinfo_kv, _feedinfo_groups)

def feedinfo(feedinfo_file = FeedInfoFile):
    return _feed_info(feedinfo_file)[0]
def feedinfo_kv(feedinfo_file = FeedInfoFile):
    return _feed_info(feedinfo_file)[1]
def feedinfo_groups(feedinfo_file = FeedInfoFile):
    return _feed_info(feedinfo_file)[2]

# FIXME: library of reading/converting/etc feedinfo, and aio_json format (from p1 -> [0] style etc)
def feedinfo_to_aiojson(feedinfo_format):
    # convert to canonical aio_json format
    aio_json = {}
    for feed in feedinfo_format:
        feed_key = feed['key']
        if not (m:=re.match(r'(p\d+)\.p\d+-(.+)', feed_key)):
            continue
        if not m.group(1) in aio_json.keys():
            aio_json[ m.group(1) ] = {}
        aio_json[ m.group(1) ][ feed_key ] = feed['last_value'] 
    return aio_json

def pump_n_to_aiojson( pump_n_format ):
    # n [1] based -> 'pn' style
    aio_json = {}
    for n,data in pump_n_format.items():
        n = f"p{n}"

        if not n in aio_json.keys():
            aio_json[ n ] = {}
        for k,value in data.items():
            if re.match(r'p\d+\.', k):
                aio_json[ n ][k] = value
            else:
                aio_json[ n ][f"p{n}.p{n}-{k}" ] = value
    return aio_json

def args_to_delta(dstring):
    dargs = {}
    if dstring:
        if m:=re.search(r'(\d+)([wdy])$', dstring):
            match m.group(2):
                case 'd':
                    dargs['days'] = int(m.group(1))
                case 'w':
                    dargs['days'] = int(m.group(1) * 7)
                case 'y':
                    dargs['days'] = int(m.group(1) * 365)
        else:
            fail(f"Expected a age time like '2d', '2w', '2y', saw {dstring}")
    return dargs

async def do_expiring(parsedargs):
    age = None

    delta = args_to_delta( parsedargs.age )

    if not delta and not parsedargs.zero and not parsedargs.all:
        # default if no other args
        delta['days'] = 7*2

    if delta:
        age = timedelta( **delta )

    def print_info(afeed):
        updated_at = datetime.strptime( afeed['updated_at'], '%Y-%m-%dT%H:%M:%SZ')
        td = ( updated_at - datetime.now())
        if parsedargs.param_only:
            print(afeed['key'])
        else:
            print( f"{afeed['key']:35s} {afeed['updated_at']:25s} {str(td):30s} {afeed['last_value'] or ''}" )
   
    zapped_or_old_params( print_info, ifage=age, ifall=parsedargs.all, ifzero=parsedargs.zero )

def zapped_or_old_params( handle_aged_feed, current_settings=FeedInfoFile, ifage=None, ifdiff=None, ifzero=False, ifall=False ):
    # For the params in feedinfo()
    # call the handle-aged-feed if:
    #   ifage: if .updated_at is too old (timedelta)
    #   ifzero: 0 | None
    #   ifall: always
    #   ifdiff: if different from element in ifdiff

    _feedinfo,dumy,groups = _feed_info(current_settings)
    def _normalize(v):
        # "normalize" a value from a feed dict
        # just means convert to number if a number
        if isinstance(v,str):
            if re.match(r'-?\d+$', v):
                return int(v)
            try:
                return float(v)
            except ValueError:
                return v
        return v

    for afeed in _feedinfo:
        # we ignore groups, of course
        if afeed['key'] in groups.keys():
            continue
        # we only are interested in pn.pn-* keys
        if not re.match(r'p\d+\.', afeed['key']):
            continue

        updated_at = datetime.strptime( afeed['updated_at'], '%Y-%m-%dT%H:%M:%SZ')
        #debug(f"consider {afeed['key']} = {afeed['last_value']}")

        show = ifall
        if ifzero:
            show |= afeed['last_value']==None or not not re.match(r'0(\.0+)?$', afeed['last_value'] )

        if ifage and (datetime.now() - updated_at > ifage):
            show |= True

        if ifdiff:
            #debug( ifdiff )

            # have to decode to [pn][pn.pn-...]
            #debug(f"  normalize key { afeed['key'] }...")
            group_key, to_key = re.match(r'(p\d+)\.p\d+-(.+)', afeed['key']).groups()
            to_key = afeed['key'] # actually, the canonical pn.pn- key
            old_value = _normalize( afeed['last_value'] )
            #debug(f"  to_key [{group_key}][ {to_key} ]")
            
            if group_key in ifdiff.keys() and to_key in ifdiff[group_key].keys():
                to_value = _normalize( ifdiff[group_key][ to_key ] )
                if old_value != to_value:
                    debug(f"New value: {to_key} = {to_value} (was {old_value})")
                    show |= True

        if show:
            handle_aged_feed( afeed )

async def do_groups(parsedargs):
    groups = feedinfo_groups()

    debug(f"eh {parsedargs}")
    if g:=parsedargs.group_key:
        for a_group in g:
            the_group = groups.get(a_group,[])
            if the_group:
                print(a_group)
                for f in the_group:
                    print(f"\t{f}")
    else:
        for g in groups:
            print(g)


async def do_describe(parsedargs):
    feed_info = feedinfo()

    if parsedargs.newest:
        feed_info = reversed( sorted( feed_info, key=lambda x: x['updated_at'] ) )

    for want in parsedargs.feeds:
        if re.search('\*', want):
            _want = want.split('*')
            want = re.compile( '^' + '.*'.join( (re.escape(x) for x in _want) ) + '$' )
            #debug(f"Want re {want}")
        for fi in feed_info:
            if isinstance(want, re.Pattern) and want.match(fi['key']) or fi['key'] == want:
                if parsedargs.list:
                    print( fi['key'] )
                elif parsedargs.fields:
                    fields = parsedargs.fields.split(',')
                    if len(fields) == 1:
                        f = fields[0]
                        print( f"{fi['key']:35s} " + (fi[f] if f in fi.keys() else 'n/a') )
                    else:
                        print( fi['key'] )
                        for f in fields:
                            print(f"\t{f} : " + (fi[f] if f in fi.keys() else 'n/a'))

                else:
                    print( json.dumps( fi, indent=2, sort_keys=True ))

async def do_repair(parsedargs):
    if os.path.splitext( parsedargs.aio_json )[1] != '.json':
        fail(f"Expected a .json file for the aio params (convert with tools/aio-param-conversion), saw: {parsedargs.aio_json}")

    delta = args_to_delta( parsedargs.older )
    if not delta:
        delta['days'] = 2*7
        print(f"# Default --older {delta}")
    delta = timedelta( **delta ) if delta else None

    # tolerate feedinfo or [0] or aio-json style (from tools/aio-param-conversion)
    with open( parsedargs.aio_json, 'r') as fh:
        aio_json = json.load(fh)
    if isinstance(aio_json,list):
        aio_json = feedinfo_to_aiojson( aio_json )
    elif not list(aio_json.keys())[0].startswith('p'):
        aio_json = pump_n_to_aiojson( aio_json )

    print(f'# Params {reduce(lambda x,y:x+len(y), aio_json.values(), 0) } out of {len(aio_json)} pumps')

    # write the canonical form ("pump_n")
    djf_dir, djf_base = os.path.split( parsedargs.aio_json )
    djf_bse,dumy = os.path.splitext( djf_base )
    desired_json_file = f"{djf_dir}/.{djf_base}-pump-n.json"
    with open(desired_json_file,'w') as fh:
        fh.write(json.dumps(aio_json,indent=2))
        fh.write("\n")

    # The current aio settings
    print(f"# AIO: {aio_key()['IO_USERNAME']}")
    current_file = None
    if not parsedargs.current:
        await do_get_feed_info(None)
        current_file = FeedInfoFile
    else:
        candidates = { AIOJSONFile : None, FeedInfoFile : None }
        for c in candidates.keys():
            if os.path.isfile(c):
                candidates[c] = os.stat(c).st_mtime
        current_file = next(reversed(sorted( candidates.items(), key=lambda nm: nm[1] if nm[1] else datetime.now() )))[0]
    print(f"# Current was in {current_file} (out of date if we published any)")

    # list of 0/null/old params
    keys_to_update = []
    def capture_zapped( afeed ):
        #debug(f"needs update {afeed['key']} : {afeed['last_value']}")
        if re.match(r'p\d+\.p\d+-x', afeed['key']):
            return
        if re.match(r'p\d+\.p\d+-', afeed['key']):
            keys_to_update.append( afeed['key'] )
    zapped_or_old_params( capture_zapped, current_settings=current_file, ifdiff=aio_json, ifage=delta, ifzero=False )

    debug(f"to update: {len(keys_to_update)} : {keys_to_update}")

    feed_info_kv = feedinfo_kv(current_file)

    #debug(aio_json)
    new_values={}
    warnings = []
    for k in keys_to_update:
        pump_key = re.match(r'(p\d+)\.', k).group(1)
        if not k in aio_json[pump_key]:
            old_v = feed_info_kv[k]
            if old_v==None or (isinstance(old_v,str) and (old_v=='' or re.match(r'-?[\d.]+$', old_v) and float(old_v)==0.0)) or old_v == 0:
                fail(f"Expected a k:v to update AIO (the v is zapped and no value in your new .json) [{pump_key}][{k}] = {old_v.__class__.__name__} {old_v} in {parsedargs.aio_json}")
            else:
                warnings.append( f"Warning: Updating (no k in your .json, and k was --older?) using the old value [{pump_key}][{k}] = {old_v.__class__.__name__} {old_v}" )
                #sys.stderr.write( warnings[-1] + "\n" ) # Wait till end to give warnings
            v = old_v
        else:
            v = aio_json[pump_key][k]
        #debug(f"## change {pump_key} {k} -> {v}")
        new_values[k] = v

    send_interval = 60.0 / MaxAIORate + 0.1
    sys.stderr.write(f"## To change {len(new_values)} { len(new_values)/MaxAIORate } minutes, 1 every {send_interval} secs\n")

    # put user/pass in config
    mqtt_config['username'] = aio_key()['IO_USERNAME']
    mqtt_config['password'] = aio_key()['IO_KEY']

    start = datetime.now()
    ct = 0
    #debug(f"Connect w/ { mqtt_config }")
    async with mqtt.Client(
        **mqtt_config,
        ) as client:
        
        for k,v in new_values.items():
            topic = f"{mqtt_config['username']}/feeds/{k}"
            debug(f"## publish {ct}/{len(new_values)} {topic} -> {v}")
            if not parsedargs.n:
                await client.publish(topic, v, qos=2)
            ct += 1
            if parsedargs.limit and ct >= parsedargs.limit:
                break
            if not parsedargs.n:
                await asyncio.sleep( send_interval )

    actual_elapsed = datetime.now() - start
    # reiterate warnings
    sys.stderr.write("\n".join(warnings) + "\n")
    sys.stderr.write(f"## Published {ct} in {actual_elapsed} secs\n")


async def main():
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    subparsers = parser.add_subparsers(help=f'sub-commands. try {sys.argv[0]} $subcommand --help')
    parser.epilog = "See also, tools/aio-param-conversions"

    subp = subparsers.add_parser('get-feed-info', help=f"Do this first (others use this file). Get the feed-info list -> {FeedInfoFile}")
    subp.set_defaults(func=do_get_feed_info)
    subp = subparsers.add_parser('fetch', help=f"Alias for get-feed-info")
    subp.set_defaults(func=do_get_feed_info)

    subp = subparsers.add_parser('groups', help=f"List groups (try `describe` for searching")
    subp.set_defaults(func=do_groups)
    subp.add_argument('group_key', nargs='*', help="List the members of this group (only list groups otherwise)")

    subp = subparsers.add_parser('describe', help=f"Describe the feeds in {FeedInfoFile}")
    subp.set_defaults(func=do_describe)
    subp.add_argument('feeds', nargs='+', help="these feed keys, * works for a pattern")
    subp.add_argument('--list', '-l', help="just list (i.e. search), don't fully describe", action='store_true')
    subp.add_argument('--fields', '-f', help="only these fields,fields,... in description (try 'last_value')", type=str)
    subp.add_argument('--newest', help="sort by .updated_at descending", action='store_true')

    subp = subparsers.add_parser('repair', help=f"Upload params that are different (just show with `-n`)")
    subp.set_defaults(func=do_repair)
    subp.add_argument('aio_json', help="(new) Parameters, in 'AIO json' format { 'p1':{'p1.p1-blah':value,...}}. Use `tools/aio-param-conversion` to convert")
    subp.add_argument('--current', help="assume the .aio.json|{FeeedInfoFile} is current (don't re-fetch)", action='store_true')
    subp.add_argument('--limit', help="do the first n updates",type=int)
    subp.add_argument('--older', help="also include params that are older than this (2d, 2w, etc). cf. `--- expiring --age x`",type=str)
    subp.add_argument('-n', help="don't actually publish, just note", action='store_true')

    subp = subparsers.add_parser('expiring', help=f"Show the expiring data in {FeedInfoFile}. --zero and --age 'or'")
    subp.set_defaults(func=do_expiring)
    subp.add_argument('--fetch', help="first, fetch new {FeedInfoFile}",action='store_true')
    subp.add_argument('--param-only','-p', help="Just print the param name",action='store_true')
    subp.add_argument('--zero', '-0', help="Or those with a value of 0, not just those that are expiring",action='store_true')
    subp.add_argument('--age', help="Or those older than this amount (default '2w'): w=weeks,d=days",type=str)
    subp.add_argument('--all', help="Show all",action='store_true')

    args = parser.parse_args()
    if 'func' not in dir(args):
        parser.print_usage()
        fail(f"# need a command")

    await args.func( args )

asyncio.run(main()) # only need async for an mqtt thing
