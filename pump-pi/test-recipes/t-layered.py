recipe = repeat('Top Repeat {}', 2,
    parallel('2nd parallel',
        sequential('Seq 1',
            comment('Com 1'),
            repeat('Inner repeat', 2,
                pause(0.1),
            )
        ),
        sequential('Seq 1',
            comment('Com 2'),
            pause(0.25),
        ),
    )
)

