#!/usr/bin/env python3.11

import asyncio
import sys,os
import re,json
from numbers import Number

sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
import asyncio_mqtt as mqtt # to make Topic etc
import paho.mqtt as paho # some constants

import agent
from simple_debug import debug

import time
from every.every import Every

class MLStepper(agent.Agent):
    """
    We are a relative stepper logic agent: we convert ml's to steps
    We convert a pump-ml to a serial-stepper command,
    We send a { ... finished:True } when the ml is done
    We just convert a serial-stepper done to a pump-ml done, we don't wait for it.
    """

    def __init__(self):
        super().__init__()
        self.target_queue = asyncio.Queue(10) # { rate:, ml: }
        self.ml_per_step = 1 # needs to be set by someone FIXME: default config...

    mqtt_config = {
        "device" : "pumps-ml",
        "name" : "pumpbox1",

        "connection" : {
            "hostname" : "localhost",
            "port" : 1884,
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
        }
    }

    async def listener(self):

        # default for "get"
        await super().listener()

        # commands
        # local/commands/devices/stepper/pumpbox1
        # FIXME: should publish these as a guide to messages:

        asyncio.gather(
            # /steps_per_ml { value:... }
            self._subscribe( self.topic_for_command(postfix='steps_per_ml'), self.handle_set_value ),
            self._subscribe( self.topic_for_command(postfix='ml_per_step'), self.handle_set_value ),
            self._subscribe( self.topic_for_command(postfix='stop'), self.handle_stop ),
            # { "ml":n, "rate":n, "pump":n }
            self._subscribe( self.topic_for_command(), self.handle_generic_command ),
            # { "ml":n, "rate":n }
            self._subscribe( self.topic_for_command('+'), self.handle_pump_command )
        )
        debug(f"Listening for commands")

    async def cleanup(self, e):
        debug("TERMINATING")
        # FIXME: signal w/a send?
        await super().cleanup(e)

    async def handle_stop(self, message):
        await self.mqtt_client.publish('local/commands/stepper/pumpbox1/stop')
        # FIXME: this should be the most recent progress, I guess we have to fetch it?
        # that causes serial-stepper to emit a finished, pos=0 for each moving motor
        # which causes us to echo it
        # What should this be?
        await self.mqtt_client.publish( self.topic_for_event(), json.dumps( { 'reason':'stopped' }) )

    async def handle_set_value(self, message):
        # FIXME: this seems like it may appear in multiple agents
        if not (m:= re.search(r'/([^/]+)$', str(message.topic))):
            debug(f"Unexpected set command {message.topic} -> {message.payload}")
            return

        if not isinstance(message.payload,dict) or 'value' not in message.payload.keys():
            debug(f'no "value" key for set command {message.topic} -> {message.payload}')
            return
        value = message.payload['value']

        property = m.group(1)
        allowed = { 'steps_per_ml':float, 'ml_per_step':float }
        property_type = allowed.get(property,None)
        if not property_type:
            debug(f"Unexpected property for set command {message.topic} -> {message.payload} (allowed {allowed})")
            # FIXME: send and event w/error?
        if not isinstance(value,property_type):
            debug(f"Unexpected type of value for set command {message.topic} -> {message.payload} (allowed {allowed})")
            # FIXME: send and event w/error?

        if property_type==int:
            # tolerate float looking things for int
            value=int(value)
        
        # FIXME a regular alias mapping thing...
        if property == 'steps_per_ml':
            property = 'ml_per_step'
            value = 1/value

        debug(f"Set property {property}={value}")
        setattr(self, property, value)

    async def handle_generic_command(self, message):
        # pump # not in topic: local/commands/ml-pumps/pumpbox1
        if 'pump' in message.payload.keys():
            # rebuild topic as if including pump#
            message.topic = mqtt.Topic( str(message.topic) + f"/{ message.payload['pump'] }" )
            #debug(f"command> {message.topic} {message.payload}")
            await self.handle_pump_command(message)
        else:
            debug(f"No handler for command {message}")

    async def handle_pump_command(self, message):
        # convert the ml-command for stepper/serial, and start it
        # setting up a listener to convert stepper/serial progress to our progress

        #  .../$pump { rate:, ml: }
        # We ignore the payload pump:
        if not (m:=re.search(r'/(\d+)$', str(message.topic))):
            debug(f"Unexpected command: {message.topic} -> {message.payload}")
            return

        pump = m.group(1)
        if pump:
            pump = int(pump)
            debug(f"in command pump[{pump}]: {message.payload}")
            
            ml = message.payload.get('ml')
            rate = message.payload.get('rate')
            if not isinstance(ml, Number) or not isinstance(rate, Number):
                debug(f"  ml-pump command expects rate: and ml:, saw {message.payload}")
                return

            # convert
            steps = ml / self.ml_per_step
            # rate is: ml/sec -> steps/sec
            hz = rate / self.ml_per_step
                
            # setup the "progress" and "finished" handler
            # We might see a spurious message (a finish before we've sent our command)

            # IMMEDIATE short-circuits the "finished" and does NOT send the pump command
            # SHORT short-circuits and does send the pump command
            # For both, the value of the env var is the number of messages [and interval between], DEBUGIMMEDIATE=3/0.6
            # The interval defaults to 1, but for msg_count=1, that has no chance to do an interval
            if count_and_interval:=(os.getenv('DEBUGIMMEDIATE',None) or os.getenv('DEBUGSHORT',None)):
                msg_count,*interval = count_and_interval.split('/')
                msg_count=int(msg_count)
                if interval:
                    interval=float(interval[0])
                else:
                    interval = 1
                debug(f"short-circuit {msg_count} messages, at {interval} secs")

                # short circuit, fake immediate response

                async def fake_progress():
                    # Do the progress/finish messages

                    # send progress for ...n-1 steps
                    # with the pause interval after each
                    for i in range(0,msg_count-1):
                        await self.handle_plain_pump_progress(mqtt.Message(
                            topic = self.topic_for_plain_pump( self.topic_for_event(pump) ),
                            payload = {"hz":hz, "pos":int(steps/msg_count * i), 'finished':False},
                            qos=0, retain=False,mid=0,properties=None
                        ))
                        await asyncio.sleep(interval)

                    # send finished
                    await self.handle_plain_pump_progress(mqtt.Message(
                        topic = self.topic_for_plain_pump( self.topic_for_event(pump) ),
                        payload = {"hz":hz, "pos":int(steps), 'finished':True},
                        qos=0, retain=False,mid=0,properties=None
                    ))

                asyncio.create_task( fake_progress() )

                if os.getenv('DEBUGIMMEDIATE',None):
                    return
                debug(f"SHORTED, should publish")
            else:
                await self._subscribe( self.topic_for_plain_pump(self.topic_for_event(pump)), self.handle_plain_pump_progress )

            # send plain-pump command
            debug(f"publishing...")
            await self.mqtt_client.publish( 
                self.topic_for_plain_pump( self.topic_for_command(pump) ), 
                json.dumps( {"hz":hz, "steps":steps} ), 
                retain=message.retain, # FIXME: maybe not?
                qos=1 # FIXME: message.qos?
            )
            debug(f"...published")

        else:
            debug(f"No handler for pump command w/o pump {message.topic} {message.payload}")

    def topic_for_plain_pump( self, mytopic ):
       return re.sub( '/' + re.escape(self.mqtt_config['device']) + '/', f"/stepper/" , mytopic, 1 )

    async def handle_plain_pump_progress(self, message):
        #debug(f"##! {message.topic} {message.payload}")

        #mytopic = re.sub( '/stepper/', f"/{self.mqtt_config['device']}/" , str(message.topic), 1 )
        pump_n = re.search(r'/(\d+)$', str(message.topic) ).group(1)
        mytopic = self.topic_for_event( postfix = f"{pump_n}" )

        if message.payload.get('finished'):
            debug(f"finished {message.topic}")
            if not ( os.getenv('DEBUGIMMEDIATE',None) or os.getenv('DEBUGSHORT',None) ): # if we hacked "immediate" then we didn't subscribe
                await self._unsubscribe( str(message.topic) )

        payload = message.payload
        hz = message.payload.pop('hz', None)
        steps = message.payload.pop('pos', None)
        if not isinstance(hz, Number) or not isinstance(steps, Number):
            debug(f"  ml-pump event expects hz: and steps:, saw {message.topic} {message.payload}")
            return

        # convert
        ml = steps * self.ml_per_step
        # FIXME: calculate actual rate?

        payload |= { "ml":ml }
        
        # echo the message: serves as progress|finish echo
        await self.mqtt_client.publish( 
            mytopic,
            json.dumps( payload ), 
            retain=message.retain, # FIXME: maybe not?
            qos=1 # FIXME: message.qos?
        )

    async def loop(self, first_time):
        # required. can simply loop forever if everything is handled in listener() and handle_*()'s
        while(True):
            await asyncio.sleep(1e6)

    def argparse_add_arguments(self, parser ):
        parser.epilog = "env DEBUGSHORT=1 ... # short circuit: fake immediate response, no stepper commands\nenv DEBUGIMMEDIATE=1 # fake immediate response, but does send stepper commands"

MLStepper().async_run()
