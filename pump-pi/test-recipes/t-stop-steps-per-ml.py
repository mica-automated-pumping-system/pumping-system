async def stopper(a_step):
    # stops before 0ml FIXME: change this when we are capturing progress from pump_ml
    await a_step.lib_agent.mqtt_client.publish( a_step.lib_agent.topic_for_stop(), '' )
    await asyncio.sleep(0.1) # mqtt round-trip

# Run ml-stepper with:
# env DEBUGSHORT=1 agents/ml-stepper.py

recipe = patch_recipestep_call(steps_per_ml(1, 86.0), stopper)
