import json
with open('config/aio-command-key.json') as fh:
    CommandAccessKey = json.load(fh)['CommandAccessKey']

aio_forward(
    # From aio
    './f/black-lightning.command',
    # you have to send along the key, i.e. password
    # { access_key : token/password, topic: "category/sub-device/etc" }
    (lambda message: message.payload.get('access_key', None) == CommandAccessKey and 'topic' in message.payload.keys() ),

    # To local
    # compose the topic
    (lambda message: f'local/commands/admin/{message.payload["topic"]}'), # topic
    (lambda message,p: p.pop('access_key') and p.pop('topic') and p) # transformed payload
)
