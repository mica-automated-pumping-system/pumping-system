#!/usr/bin/env python3.11
"""
An agent for running recipes written with a DSL like thing. Exits when the recipe finishes.
    parallel, sequential, extendable dsl

    % recipe/dsl.py bare-dsl.py

the bare-dsl.py should look like this:

    # base-dsl.py
    any code...


    recipe = blah using the dsl...
Where `blah...` should give an awaitable, or a RecipeStep.

Simplest:
    # bare-dsl.py
    recipe = comment("I send a comment")

What's happening?
    * we setup some globals as if: 
        import * from recipe/dsl.py
        IsResume = true if --resume|--resumex # flag
    * we compile & exec your file
    * we look for a `recipe` variable that you made (top level)
    * if it is an awaitable, we await it; if it is a RecipeStep, we await it()

If you ever get an exception "cannot reuse already awaited coroutine",
then you probably passed an awaitable() into something, and that something got reused (inside a repeat?).
Wrap it in lambda (or nested def), to allow multipe await's on it:
async def mysomething( awaitable ):
    await awaitable()
mysomething( lambda: myawaitable() )
"""

"""
FIXME
    As we start each line, announce it blah/recipe/at { start: n, duration:xx }
    As we finish each line, announce it blah/recipe/at { finished: n, elapsed:xx }
    so have to manage that state per parallel fork
"""

import asyncio
import os,sys,inspect,time,json,re,traceback,types
from copy import copy
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )

import paho.mqtt as paho # some constants

from simple_debug import debug 
import agent

from recipe_dsl import RecipeStep
# FIXME: find and import any other recipe-dsl libs
import recipe_dsl_maps,recipe_dsl

"""
need to be "async def" if they appear in a parallel/sequential
"""
class MapsDSLExec(agent.Agent):
    """We execute one script, then exit(0).
    someone else has to start us
    """
    mqtt_config = {
        "device" : "maps-dsl-executor",
        "name" : "$dsl-scriptname.py", # Nb. fixed up by us in __init__

        "connection" : { # paho mqtt args # FIXME: should be inherited from file config
            "hostname" : "localhost",
            "port" : 1884,
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
        }
    }

    def __init__(self):
        super().__init__()
        self.in_recipe = None # when we are actually in a recipe
        self.recipe_state = None
        self.recipe_file = None # set in argparse

    async def argparse(self):
        await super().argparse()

        # now we have parsed_args
        self.recipe_file = self.parsed_args.recipe
        self.mqtt_config['name'] = os.path.basename( self.recipe_file ) # FIXME: how much of path? because we need to be unique on this local mqtt

    def topic_for_stop(self):
        return self.topic_for_command('stop')

    def interesting_topics(self):
        """Add to topics that are useful to print out for --topics"""
        return super().interesting_topics() + [ 
            self.topic_for_stop()
        ]

    async def listener (self):
        # NB: our base topic(s) have ../$recipe appended

        await super().listener() # default behavior for mqtt .../get
        
        await self._subscribe( self.topic_for_command('stop'), self.handle_stop )

    async def handle_stop(self, message):
        if self.in_recipe:
            debug(f"cancel -> {self.in_recipe}")
            self.in_recipe.cancel()
        else:
            debug(f"Unlikely: no recipe being executed {self.in_recipe}")

    def state_file(self):
        wo_ext,ext = os.path.splitext( super().state_file() )
        return f"{wo_ext}/{self.recipe_file}.json"

    def state(self):
        return {
            'recipe' : self.recipe_file,
            'recipe_state' : self.recipe_state,
        }

    async def loop(self, first_time):
        try:
            await self._loop()
        except Exception as e:
            #debug(f"Exception in loop(): {e}")
            traceback.print_exc()
            await self.mqtt_out_queue.join()
            debug(f"send queue empty")
            await self.exit(2,e)

    async def _loop(self):

        if self.parsed_args.clear_resume:
            had_resume = False
            if os.path.isfile( self.state_file()):
                os.remove( self.state_file() )
                had_resume = True
            await self.mqtt_client.publish(
                self.topic_for_device(),
                json.dumps({"script" :  self.recipe_file, "removed_resume": had_resume }),
                retain=True
            )
            self._already_say_quit = True # prevent default quit message in this case
            await self.mqtt_out_queue.join()
            await self.exit(0)
            return
            # WE ARE DONE

        start = time.monotonic()

        # script name also is part of our topic
        start_payload = { "status" : "start", "script" :  self.recipe_file }
        debug(f"## try resume? {self.parsed_args.try_resume} state_file? {self.state_file()} ?? { os.path.isfile( self.state_file()) }")
        if self.parsed_args.try_resume and os.path.isfile( self.state_file()):
            self.parsed_args.resume = True

        if self.parsed_args.resume or self.parsed_args.resumex:
            start_payload['resume'] = True

        await self.mqtt_client.publish(
            self.topic_for_device(),
            json.dumps(start_payload),
            retain=True
        )

        # Have to set the environ before we "compile" the recipe
        # because patch_recipestep_call() runs at it's "compile" time
        if self.parsed_args.resumex:
            # to disable patch_recipestep_call
            self.parsed_args.resume == True
            os.environ['NOPATCHSTEP'] ='1'

        # setup
        RecipeStep.lib_agent = self

        # Execute the recipe code -> awaitable
        recipe = self.extract_recipe()

        # We catch compilation and recipe-building (executing the dsl) here
        # But, this does not run the recipe, just makes the awaitable
        if isinstance( recipe, types.CoroutineType ) or isinstance( recipe, asyncio.Task ):
            debug(f"...a coroutine {recipe}")
            recipe_task = recipe
        elif isinstance( recipe, RecipeStep ):
            debug(f"...a RecipeStep {recipe.__class__.__name__}")
            recipe.human_id_prefix = f"{self.recipe_file}."
            
            if self.parsed_args.resume: # or self.parsed_args.resumex:
                state = self.load_state()

                if not state:
                    raise Exception(f"Expected the saved state (\"recipe\":) to have some data ('{self.recipe_file}'), saw '{state}'")
                debug(f"## Resuming with state!")

                saved_recipe_name = state['recipe']

                if saved_recipe_name != self.recipe_file:
                    raise Exception(f"Expected the saved state (\"recipe\":) to have the same name as this recipe ('{self.recipe_file}'), saw '{saved_recipe_name}'")

                # step_name, {kwargs}, [rest]
                recipe_state = state['recipe_state']
                debug(f"Resume w/ {self.state_file()}")
                recipe.resumex( *recipe_state )

            # invokes the .__call__ method
            recipe_task = recipe() 
        else:
            raise Exception(f"Expected recipe= def asyncio|RecipeStep (in {self.recipe_file}), saw {recipe.__class__.__name__} {recipe}")

        debug(f"Start recipe!")
        reason = {}
        cancelled = False

        # Now we run it, and catch the cancel which is presumably from a /stop
        try:
            # has to be a task so we can send .cancel, we still see bubbled cancel
            self.in_recipe = asyncio.create_task(recipe_task)
            await self.in_recipe
            self.in_recipe = None
        except asyncio.exceptions.CancelledError as e:
            debug(f"Executive saw cancel!")
            if isinstance( recipe, RecipeStep ):
                self.recipe_state = recipe.state()
                # FIXME: probably provide a /get for the state
                self.save_state()
                
            else:
                await self.mqtt_client.publish(
                    self.topic_for_event('recovery'), # FIXME: maybe just part of the publish below
                    json.dumps({ '#': f"Can't save state for recovery, because the recipe wasn't a RecipeStep ({recipe})"}),
                    retain=True
                )
                
            cancelled = True
            reason = { 'reason': 'stopped' }
            # nb: we consume Cancelled, and handle termination ourselves

        # done
        debug(f"# DONE {RecipeStep.step_ct} steps, total pause time {recipe_dsl.h_duration(RecipeStep.pause_time)}")
        await self.mqtt_client.publish(
            self.topic_for_event(),
            json.dumps({ "finished" : True, "elapsed" : recipe_dsl.h_duration( (time.monotonic()-start)) } | reason),
            retain=True
        )
        # in combo with .exit, I think we are sending the devices/... status twice
        await self.mqtt_client.publish(
            self.topic_for_device(),
            json.dumps({ "status" : "quit", "script" :  self.recipe_file } | reason),
            retain=True
        )

        # We do not need to do status:quit
        # that is handled by self.exit()

        # this hangs sometimes. sad. maybe because we are handling a cancel?
        if not cancelled:
            await self.mqtt_out_queue.join()

        await self.exit(0)

    def extract_recipe(self):
        # kind of like import, but pre-seed as if recipe_dsl_maps.* was imported

        # FIXME: feedback if no such file

        debug(f"Get the `recipe=...` from {self.recipe_file}")

        # compile the code
        with open(self.recipe_file, mode='r') as fh:
            # FIXME: feedback on any exception
            code = fh.read()
            # FIXME: feedback on any exception
            compiled_code = compile( code, self.recipe_file, mode='exec')
            code = None

        # pre-seed
        # FIXME: add asyncio_mqtt object, i.e. our lib/agent
        script_globals = { k: getattr(recipe_dsl_maps,k) for k in dir( recipe_dsl_maps ) if not k.startswith('_') }
        # useful
        script_globals['asyncio'] = globals()['asyncio']
        script_globals['__builtins__'] = globals()['__builtins__']
        # be nice, give it __file__
        full_recipe_file = ''
        if self.recipe_file.startswith('/'):
            full_recipe_file = self.recipe_file
        else:
            # we specifically do it relative to "here", rather than canonicalize out any symlinks
            full_recipe_file = sys.argv[0] + "/" + self.recipe_file 
        script_globals['__file__'] = full_recipe_file
        
        # some flags
        script_globals['IsResume'] = self.parsed_args.resume

        # and execute (we want it's `recipe` var)
        # FIXME: feedback on any fail
        exec( compiled_code, script_globals ) # w/o a "locals" treats the code as a module (file)
        if 'recipe' not in script_globals.keys():
            raise Exception(f"Expected 'recipe = ...' in {self.recipe_file}")

        # the presumed recipe
        return script_globals['recipe']

    def argparse_add_arguments(self, parser ):

        #import recipe_dsl_maps,recipe_dsl
        recipe_dsl.argparse_add_arguments(parser)
        #recipe_dsl_maps.argparse_add_arguments(parser)

        parser.add_argument('--clear-resume', help="Don't run. If there is a saved state for the recipe, delete it", action='store_true')
        parser.add_argument('--resume', help="if there is a saved state for the recipe, resume from there, else fail", action='store_true')
        parser.add_argument('--try-resume', help="if there is a saved state for the recipe, resume from there, else just run", action='store_true')
        parser.add_argument('--resumex', help="Same as --resume, but disables disable patch_recipestep_call(), same as env NOPATCHSTEP=1", action='store_true')
        # NB: actually handled by lib/recipe_dsl.py, the p_totals() wrapper: (FIXME?)
        parser.add_argument('--totals-only', help="Only emit the local/events/maps-dsl-executor/{recipe}/totals messge", action='store_true')
        parser.add_argument('recipe', help="run (if no other options) a dsl recipe .py (lib/recipe_dsl + lib/recipe_maps_dsl)")

# looks for filename in argparse
MapsDSLExec( ).async_run() # internally does asyncio.run( agent.run() )
