"""
Get the `connection` from config/mqtt-aio-config.py,
"""
from copy import copy

config_file = 'config/mqtt-aio-config.py'

with open(config_file, mode='r') as fh:
    # FIXME: feedback on any exception
    code = fh.read()
    # FIXME: feedback on any exception
    compiled_code = compile( code, config_file, mode='exec')
    code = None

script_globals = {}
exec( compiled_code, script_globals )
connection = script_globals['connection']
_visible = copy(connection)
_visible.pop('password',None)
print(f"## config {config_file} -> {_visible}")


