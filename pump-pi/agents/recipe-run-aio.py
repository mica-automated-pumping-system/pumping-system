#!/usr/bin/env python3.11

import asyncio
import json,re,typing,pathlib,fnmatch
from datetime import datetime
import subprocess
import sys,os
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from simple_debug import debug
import paho.mqtt as paho # some constants|--help'
import agent
class RunAIO(agent.Agent):
    """We run the latest AIO settings, or just compile, or run an old recipe, or list old recipes.
    """
    # lifecycle is:
    # SomeAgent()
    # .argparse()
    # connect with `will` based on .description
    # taskgroup.create_task( .listener( ) )
    # taskgroup.create_task( .advertise() )
    # taskgroup.create_task( .loop() )
    # await .cleanup(exception) if we catch an exception
    #   always call super().cleanup(exception) if you override

    AIORecipeDir = 'aio-recipes'
    AIOJSONDir = 'aio-json'

    mqtt_config = {
        "device" : "recipe",
        "name" : "aio-runner",

        "connection" : { # paho mqtt args
            "hostname" : "localhost",
            "port" : 1884,
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
        }
    }

    def __init__(self):
        super().__init__() # minimal setup
        os.makedirs( self.AIORecipeDir, exist_ok=True )
        self.last_run = None # recipe name

    async def argparse(self):
        # if you need to handle/look at any argparse
        await super().argparse()

    def interesting_topics(self):
        return super().interesting_topics() + [
            self.topic_for_get('list'),
            self.topic_for_command('compile'),
            self.topic_for_command('recipe-list'),
            self.topic_for_device('recipes'),
            self.topic_for_command('run'),
            self.topic_for_command('run/<recipe.py>'),
            self.topic_for_command('resume-or-run'),
            self.topic_for_command('resume-or-run/<recipe.py>'),
            self.topic_for_command('compile-and-run'),
            self.topic_for_command('stop'),
            self.topic_for_command('clear-resume'),
            self.topic_for_command('clear-resume/<recipe.py>'),
            self.topic_for_get('totals'),
            self.topic_for_get('totals/<recipe.py>')
        ]

    async def listener(self):
        # optional
        # subscribes to topics, assigning handlers
        await super().listener() # default behavior for mqtt .../get

        # your stuff:
        # FIXME: static, and adds to interesting_topics()
        await self._subscribe( self.topic_for_get('list'), self.handle_list_recipes)
        await self._subscribe( self.topic_for_command('compile'), self.handle_compile_recipe)
        await self._subscribe( self.topic_for_command('run'), self.handle_run_recipe)
        await self._subscribe( self.topic_for_command('resume-or-run/+'), self.handle_resume_run_recipe)
        await self._subscribe( self.topic_for_command('resume-or-run/+'), self.handle_resume_run_recipe)
        await self._subscribe( self.topic_for_command('clear-resume'), self.handle_clear_resume)
        await self._subscribe( self.topic_for_command('clear-resume/+'), self.handle_clear_resume)
        await self._subscribe( self.topic_for_get('totals'), self.handle_get_totals)
        await self._subscribe( self.topic_for_get('totals/+'), self.handle_get_totals)
        await self._subscribe( self.topic_for_command('compile-and-run'), self.handle_compile_and_run_recipe)
        await self._subscribe( self.topic_for_command('stop'), self.handle_stop)

    # each handler
    async def handle_list_recipes(self,message):

        # make list of recipe files
        rez = []
        with os.scandir(self.AIORecipeDir) as it:
            for f in it:
                if f.is_file():
                    if fnmatch.fnmatch(f.name, '*-recipe.py'):
                        rez.append(f.name)
        rez.sort(key=lambda x: os.path.getctime(f"{self.AIORecipeDir}/{x}"))
        rez.reverse()
        await self.mqtt_client.publish( self.topic_for_device( 'recipes'), json.dumps(rez) )

    async def handle_stop(self,message):
        if self.last_run:
            debug(f"Stopping: maps-dsl-executor {self.last_run}")
            await self.mqtt_client.publish(f'local/commands/maps-dsl-executor/{self.last_run}/stop')
            self.last_run = None
        else:
            debug(f"No last recipe, so nothing to stop")

    async def handle_compile_and_run_recipe(self, message):
        recipe = f"aio-{datetime.now().strftime('%Y-%m-%d_%H:%M') }-recipe.py"

        response = await self.compile_recipe( recipe )

        if response.get('status',None) == 'done':
            asyncio.create_task( self._handle_run_recipe(recipe, 'run-and-compile') )
        else:
            await self.mqtt_client.publish( self.topic_for_event('run-and-compile'), json.dumps( response ), retain=True )

    async def handle_compile_recipe(self, message):
        response = await self.compile_recipe( f"aio-{datetime.now().strftime('%Y-%m-%d_%H:%M') }-recipe.py" )
        await self.mqtt_client.publish( self.topic_for_event('compile'), json.dumps( response ) , retain=True)

    async def compile_recipe(self, output):
        # returns a response suitable for publishing
        response = { "recipe" : output }
        # FIXME: this pump-ct==6 should be part of request?
        #options = [ f"--{re.sub(r'_','-',o)}" for o in ('cache','use_cache') if getattr(self.parsed_args, o)]
        aio_json = f"{self.AIOJSONDir}/"
        aio_json += re.sub(r'-recipe\.py','.json', response['recipe'])
        self.ensure_directory(file=aio_json)
        options_h = { '--cache':True, '--cache-file':aio_json }
        # allow explicity cli arg override:
        options_h = options_h | { f"--{o}":getattr(self.parsed_args,) for o in ('cache','use_cache') if getattr(self.parsed_args, o)}
        options = []
        """
        OBSOLETE, but reincorporate somehow for testing node-red
        if options_h.get('--cache',False):
            options.append('--cache')
        if options_h.get('--cache-file',None):
            options.extend( ['--cache-file', options_h['--cache-file']] )
        """
        self.ensure_directory( self.AIORecipeDir )
        cmd = [ 'tools/aio-param-conversion', *options, '.aio', f"{self.AIORecipeDir}/{response['recipe']}" ]

        debug(f"Compile: {cmd}")
        rez = await asyncio.create_subprocess_exec(*cmd) # subprocess.run(cmd)
        await rez.wait()
        if rez.returncode != 0:
            response['status'] = "fail" # FIXME: need to get status etc correct
            response['exitstatus'] = rez.returncode
        else:
            response['status'] = "done"
            response['finished'] = True

        return response

    def topic_or_latest_recipe(self, message, our_base_topic):
        # get the recipe from the topic, or latest
        recipe = str(message.topic)[ len(our_base_topic)+1 : ]
        if recipe == '':
            # latest
            recipe = next(( recipe for recipe in reversed(sorted(os.listdir( self.AIORecipeDir ))) if recipe.endswith('.py')),None)
            debug(f"found latest recipe in { self.AIORecipeDir}:  {recipe}")
        else:
            # FIXME: check for extant recipe
            debug(f"try extant recipe in { self.AIORecipeDir}:  {recipe}")
            pass

        return recipe

    async def handle_run_recipe(self, message):
        # /run = run the latest that is compiled in aio-recipes/ by sort order
        # /run/$recipe = run aio-recipes/$recipe
        recipe = self.topic_or_latest_recipe( message, self.topic_for_command('run') )

        # spawn, long running
        asyncio.create_task( self._handle_run_recipe(recipe,'run') )

    async def handle_resume_run_recipe(self, message):
        # /run = run the latest that is compiled in aio-recipes/ by sort order
        # /run/$recipe = run aio-recipes/$recipe
        recipe = self.topic_or_latest_recipe( message, self.topic_for_command('resume-or-run') )

        # spawn, long running
        asyncio.create_task( self._handle_run_recipe(recipe,'resume-or-run', try_resume=True) )

    async def _handle_run_recipe(self, recipe, why):
        debug(f"Run recipe in {recipe}")

        # long running, create_task around us probably
        response = await self.run_recipe(recipe, try_resume=True)

        await self.mqtt_client.publish( self.topic_for_event(why), json.dumps( response ), retain=True )

    async def run_recipe(self, recipe, try_resume=False):
        # returns a response suitable for publishing as the reason

        self.last_run = recipe

        response = { "recipe" : f"{self.AIORecipeDir}/{recipe}" }

        if not recipe:
            response['status'] = "fail" # FIXME: need to get status etc correct
            response['reason'] = "no latest aio-recipe, try compile first"
        elif not os.path.isfile( response['recipe'] ): 
            response['status'] = "fail" # FIXME: need to get status etc correct
            response['reason'] = f"no such aio-recipe, try list: {recipe}"

        else:

            # FIXME: since we were asked to "run", we should send a message saying "start"
            #   an echo of local/devices/maps-dsl-executor/$recipe {"status": "start", "script": "aio-recipes/$recipe"}

            cmd = []
            if self.parsed_args.fake_sleep:
                cmd.extend( ['env', 'DEBUGFAKESLEEP=1'] )
            cmd.append( 'tools/run-dsl-recipe.py' )
            if try_resume:
                cmd.append('--try-resume')
            cmd.append( response['recipe'] )

            debug(cmd)
            subprocess = await asyncio.create_subprocess_exec( *cmd )
            await subprocess.wait()
            # This, of course, blocks the task
            returncode = subprocess.returncode # await subprocess.wait()
            if returncode != 0:
                response['status'] = "fail" # FIXME: need to get status etc correct
                response['exitstatus'] = returncode
            else:
                response['status'] = "done"
                response['finished'] = True

        return response

    async def handle_clear_resume(self, message):
        # clears the resume state of the recipe (if any)

        recipe = self.topic_or_latest_recipe( message, self.topic_for_command('clear-resume') )

        response = { "recipe" : f"{self.AIORecipeDir}/{recipe}" }

        if not recipe:
            response["removed_resume"] = False
            response['reason'] = "no latest aio-recipe, try compile first"
        elif not os.path.isfile( response['recipe'] ): 
            response["removed_resume"] = False
            response['reason'] = f"no such aio-recipe, try list: {recipe}"

        else:

            # we just run tools/run-dsl-recipe.py --clear-resume

            # FIXME: 
            #   an echo of local/devices/maps-dsl-executor/aio-2023-02-20_plausible.py.json {"script": "aio-recipess/aio-2023-02-20_plausible.py.json", "removed_resume": false}

            cmd = [ 'tools/run-dsl-recipe.py', '--clear-resume', response['recipe'] ]
            debug(f"Cmd: {cmd}")
            subprocess = await asyncio.create_subprocess_exec( *cmd )
            await subprocess.wait()
            # This, of course, blocks the task
            returncode = subprocess.returncode
            if returncode != 0:
                response["removed_resume"] = False
                response['exitstatus'] = returncode
            else:
                response["removed_resume"] = True

        await self.mqtt_client.publish( self.topic_for_event('clear-resume'), json.dumps( response ), retain=False )

    async def handle_get_totals(self, message):
        # Only send the totals of the recipe

        debug(f"Start get totals...")
        recipe = self.topic_or_latest_recipe( message, self.topic_for_command('clear-resume') )
        self.last_run = recipe

        response = { "recipe" : f"{self.AIORecipeDir}/{recipe}" }

        if not recipe:
            response['status'] = "fail" # FIXME: need to get status etc correct
            response['reason'] = "no latest aio-recipe, try compile first"
        elif not os.path.isfile( response['recipe'] ): 
            response['status'] = "fail" # FIXME: need to get status etc correct
            response['reason'] = f"no such aio-recipe, try list: {recipe}"

        else:

            # we just run tools/run-dsl-recipe.py --clear-resume

            # FIXME: 
            #   an echo of local/devices/maps-dsl-executor/aio-2023-02-20_plausible.py.json {"script": "aio-recipess/aio-2023-02-20_plausible.py.json", "removed_resume": false}

            cmd = [ 'tools/run-dsl-recipe.py', '--totals-only', response['recipe'] ]
            debug(f"Going to {cmd}")
            subprocess = await asyncio.create_subprocess_exec( *cmd )
            await subprocess.wait()
            # This, of course, blocks the task
            returncode = subprocess.returncode # await subprocess.wait()
            if returncode != 0:
                response['status'] = "fail" # FIXME: need to get status etc correct
                response['exitstatus'] = returncode
            else:
                response['status'] = "done" # FIXME: need to get status etc correct
                response['totals-sent'] = True

        debug(f"Done get-totals...")
        await self.mqtt_client.publish( self.topic_for_event('get-totals'), json.dumps( response ), retain=False )

    async def loop(self, first_time):
        # required. can simply loop forever if everything is handled in listener() and handle_*()'s
        while(True):
            await asyncio.sleep(1e6)

    def argparse_add_arguments(self, parser ):
        parser.add_argument('--fake-sleep', help="tell the tools/run-dsl-recipe.py to fake sleep 1/10 second", action='store_true')
        parser.add_argument('--cache', help="after fetching AIO settings, cache it as .aio.json, then continue", action='store_true')
        parser.add_argument('--use-cache', help="instead of the AIO settings, read the cache'd .aio.json, then continue", action='store_true')
        parser.epilog = 'For debug/short-circuiting, see tools/run-dsl-recipe.py --help'

RunAIO().async_run() # internally does asyncio.run( agent.run() )

'''
How To Exit
To do a clean exit, which nicely kills off an async tasks, and sends the 'quit' status.
Should be able to call this from any `def async`.

await self.exit($statuscode, $exception-if-you-have-it)
return # uh... cause this whole chain of awaitables to die...?
'''
