#!/usr/bin/env python3.11
# try --help
# see AIOStatusUpdater as main class

import asyncio
import sys,os,json,re,datetime,typing,traceback
from pathlib import Path
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from simple_debug import debug
import paho.mqtt as paho # some constants|--help'
import agent
import rate_limiter
from mqtt_aio_config import connection as mqtt_aio_connection

module_dir = re.sub(r'\.py$','', __file__)

class ForwardSupportMixin:
    """For the `forward` dsl and behavior
        Registers the forwarding, 
    """
    def __init__(self, *args, **kwargs):
        self.by_module = {} # module-path : [ list of topics it is forwarding ]
        self.ratelimiter = None # add one if you need it
        super().__init__(*args, **kwargs)

    def setup_forward(self, in_topic, predicate, out_topic, transform, priority=False ):
        # We accumulate the forwardings because we want to be able to say them at --topics time
        # without having already subscribed
        #
        # called by the module: local_forward( ... )
        # handled by _forwarding() w/same args
        debug(f"Setup {self.__class__.__name__} {self._forwarding_via}")
        debug(f"  {in_topic} -> {out_topic}")

        # by module so we can un-load by module
        if not self._forwarding_via  in self.by_module.keys():
            self.by_module[ self._forwarding_via ] = []
        self.by_module[ self._forwarding_via ].append( 
            {
            'in_topic':in_topic, 'predicate' : predicate,
            'out_topic':out_topic, 'transform' : transform,
            'priority':priority
        } )

    def interesting_forwardings(self):
        forwardings = []
        for inout_list in self.by_module.values():
            for inout in inout_list:
                forwardings.append( f"{inout['in_topic']} -> {inout['out_topic']}" )
        return forwardings

    def fixup_forward_topic(self, topic):
        if next((x for x in (x for x in self.__class__.__mro__ if x != self.__class__) if '.fixup_forward_topic' in dir(x)),None):
            return super().fixup_forward_topic(topic)
        else:
            return topic

    async def subscribe_forwarders(self, taskgroup):
        def xxsubscribe(tg, inout):
            # aaaarg, closure over the arguments because no block scope in python
            tg.create_task( self._subscribe( self.fixup_forward_topic(inout['in_topic']), lambda message: self._forwarding(message, **inout ) ) )

        for inout_list in self.by_module.values():
            for inout in inout_list:
                debug(f"Subscribe: {inout['in_topic']} {inout}")
                xxsubscribe(taskgroup, inout)

    async def _forwarding(self, message, in_topic, predicate, out_topic, transform, priority=False):
        # handles messages that might be forwarded
        # module local_forward(...) -> self.setup_forward -> loop -> foreach self.by_module -> subscribe w/lambda

        # we don't want old messages, just current
        if message.retain:
            return

        debug(f"# IN {message}, pred {predicate} priority {priority}")
        #debug(f"  out: {out_topic} / trans {transform}")
        if self.ratelimiter and not self.ratelimiter.allowed(extra = 5 if priority else 0):
            debug(f"Rate Limit")
            #debug(f"   for {message.topic}")
            return

        def catching( fn, fail_prefix, *args, **kwargs ):
            # pattern for try/fail report
            # returns the value or Throws on failure
            try:
                return fn( *args, **kwargs )
            except Exception as e:
                # just give up on errors
                # Should notify someone so we'll clean it up later
                print("During out_topic transform")
                traceback.print_exception(e)
                debug("# Not Forwarded!")
                self.queue_a_message( self.topic_for_event("errors"), ["During out_topic transform"] + traceback.format_exception(e), retain=True )
                raise Exception("--PLUGIN FAILED--")

        try: # for the catching() patterns

            # FIXME: catch errors from predicate() and transform(). print them, mqtt them, and unsubscribe the topic 
            pred = catching( predicate, 'During predicate', message )

            if not pred:
                debug(f"  but doesn't match predicate: {message.payload}")
            else:
                debug(f"  inpayload {message.payload}")

                # Transorm topic
                if isinstance(out_topic,typing.Callable):
                    out_topic = catching( out_topic, 'During out_topic transform', message )

                # Transform the payload
                new_payload = catching( transform, 'During payload transform', message, message.payload )

                #debug(f"  match predicate! transformed {new_payload}")
                #debug(f"  send {self.other_side.mqtt_config['connection']['hostname']}: {self.other_side.fixup_forward_topic(out_topic)}")
                if os.getenv('NOAIO',None):
                    debug(f"## PUBLISH {datetime.datetime.now()} {self.other_side.fixup_forward_topic(out_topic)} {json.dumps( new_payload )}")
                else:
                    try:
                        await self.other_side.mqtt_client.publish( self.other_side.fixup_forward_topic(out_topic), json.dumps( new_payload ), retain=False, qos=0 )
                    except mqtt.error.MqttCodeError as e:
                        if 'The client is not currently connected' in str(e):
                            await asyncio.sleep(5) # time to fix wifi?
                            await self.reconnect()

        except Exception as e:
            if str(e).startswith("--PLUGIN FAILED--"):
                return # was skipped
            else:
                raise e


class AIOStatusUpdater_AIOSide(ForwardSupportMixin, agent.Agent):
    """We transform some local mqtt to AIO status:
    for topic w/payload-pattern, transform, send to AIO w/rate-limit.
    We rate limit to 10/minute with high-priority allowed to 15/minute
    Rates are in config.

    Forwarding is defined viai modules:
    agents/aio-status-updater/*.py
    """
    # lifecycle is:
    # SomeAgent()
    # .argparse()
    # connect with `will` based on .description
    # taskgroup.create_task( .listener( ) )
    # taskgroup.create_task( .advertise() )
    # taskgroup.create_task( .loop() )
    # await .cleanup(exception) if we catch an exception
    #   always call super().cleanup(exception) if you override

    # the local mqtt
    mqtt_config = {
        'connection' : mqtt_aio_connection,
    }

    def __init__(self, other_side):
        super().__init__() # minimal setup
        self.other_side = other_side # so we can forward

    async def argparse(self):
        # if you need to handle/look at any argparse
        await super().argparse()

    def interesting_topics(self):
        """Add to topics that are useful to print out for --topics"""
        return (
            # no default interesting
            self.interesting_forwardings() # from mixin
        )

    async def advertise(self ):
        # Override the default behavior
        #await self.mqtt_client.publish( self.topic_for_device(), json.dumps( self.description() ), retain=True, qos=1 )
        # debug(f"FIXME: advertise something to aio to some topic?")
        pass # don't !

    async def listener(self):
        # optional
        # subscribes to topics, assigning handlers
        # We `await yourhandler`, so other messages won't be handled till you finish.
        # Maybe use asyncio.create_task() to spawn long-running things.
        # Thus, by default, there are no race-conditions between handlers,
        # (but there would be if you create_task())
        
        # Specifically do NOT listen to anything by default

        # your stuff:
        #await self._subscribe( self.topic_for_get(), self.handle_local_get_devices ) 
        pass

    # each handler
    async def handle_local_get_devices(self, message):
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        raise Exception("xImplement things like me")

    async def loop(self, first_time):
        # required. can simply return if everything is handled in listener() and handle_*()'s
        
        # setup
        # start async work stuff
        debug(f"AIO Side doing nothing")

        async with asyncio.TaskGroup() as tg:
            await self.subscribe_forwarders(taskgroup=tg)

        while(True):
            await asyncio.sleep(1)

    async def cleanup(self, e):
        await super().cleanup(e)

        print(f"do your cleanup, like GPIO.cleanup(), because we are exiting because {e}")

    def fixup_forward_topic(self, topic):
        """Allow 
            full topic: blah/blah/blah, it better be the full aio topic
            user-relative: ./feeds/blah/blah, we prefix the mqtt_config['username']
        """
        if topic.startswith('./'):
            topic = re.sub(r'.', self.mqtt_config['connection']['username'], topic, 1)
        return topic

class AIOStatusUpdater(ForwardSupportMixin, agent.Agent):
    # "main" class
    """We transform some local mqtt to AIO status:
    for topic w/payload-pattern, transform, send to AIO w/rate-limit.
    We rate limit to 10/minute with high-priority allowed to 15/minute
    Rates are in config.

    agents/aio-status-updater/*.py are modules
    """
    # lifecycle is:
    # SomeAgent()
    # .argparse()
    # connect with `will` based on .description
    # taskgroup.create_task( .listener( ) )
    # taskgroup.create_task( .advertise() )
    # taskgroup.create_task( .loop() )
    # await .cleanup(exception) if we catch an exception
    #   always call super().cleanup(exception) if you override

    # the local mqtt
    mqtt_config = {
        "device" : "aio-status-updater",
        "name" : "default-aio",
    }

    def __init__(self):
        super().__init__() # minimal setup

        self._sorttopics = False # we'll sort the topics

        self.ratelimiter = RateLimiter(10, 60)
        
        # who we/they forward to
        self.other_side = AIOStatusUpdater_AIOSide(other_side=self)

        # load our modules
        for module in sorted( Path( module_dir ).glob('*.py') ):
            debug(f"Forwarder {module}")
            self.add_module( module )

    def add_module(self, module):
        with open(module, mode='r') as fh:
            # FIXME: feedback on any exception
            code = fh.read()
            # FIXME: feedback on any exception
            compiled_code = compile( code, module, mode='exec')
            code = None
        script_globals = { 
            'local_forward': self.setup_forward, 
            'aio_forward' : self.other_side.setup_forward, 
            're' : re, 
            'debug':debug 
        }
        
        # should only be dynamically relevant see the ForwardSupportMixin.setup_forward()
        self._forwarding_via = module
        self.other_side._forwarding_via = module

        exec( compiled_code, script_globals )

    async def argparse(self):
        # if you need to handle/look at any argparse
        await super().argparse()

    def interesting_topics(self):
        """Add to topics that are useful to print out for --topics"""
        return (
            sorted(super().interesting_topics())
            + sorted(self.interesting_forwardings()) # from mixin
            + sorted(self.other_side.interesting_topics())
            )

    async def listener(self):
        # optional
        # subscribes to topics, assigning handlers
        # We `await yourhandler`, so other messages won't be handled till you finish.
        # Maybe use asyncio.create_task() to spawn long-running things.
        # Thus, by default, there are no race-conditions between handlers,
        # (but there would be if you create_task())
        await super().listener() # default behavior for mqtt .../get

        # your stuff:
        await self._subscribe( self.topic_for_get(), self.handle_local_get_devices ) 

    # each handler
    async def handle_local_get_devices(self, message):
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        await self.exit(1, Exception("Implement things like me"))

    async def loop(self, first_time):
        try:
            async with asyncio.TaskGroup() as tg:
                debug(f"# Spawn AIOStatusUpdater_AIOSide")
                tg.create_task( self.other_side.run() )

                await self.subscribe_forwarders(taskgroup=tg)

            while(True):
                await asyncio.sleep(1)

        except Exception as e:
            await self.other_side.exit(0)
            await self.exit(1, e)
            return

    async def cleanup(self, e):

        # cleanup the aio-side
        if self.other_side:
            await self.other_side.exit(1, None)

        await super().cleanup(e)

        #print(f"do your cleanup, like GPIO.cleanup(), because we are exiting because {e}")


# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
AIOStatusUpdater( ).async_run() # internally does asyncio.run( agent.run() )
