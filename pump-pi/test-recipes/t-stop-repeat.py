async def stopper(a_step):
    await a_step.lib_agent.mqtt_client.publish( a_step.lib_agent.topic_for_stop(), '' )
    await asyncio.sleep(0.1) # mqtt round-trip

recipe = repeat("Repeat Test {}", 2,
    comment("Comment 1"),
    patch_recipestep_call(comment("Comment 2"), stopper ) # ONLY first repeat
)
