#!/usr/bin/env python3.11

import asyncio
import sys,os,json,re,atexit,time
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
from simple_debug import debug
import paho.mqtt as paho # some constants|--help'
import RPi.GPIO as GPIO


# write your
class UIButtoner():
    """
    Just tests the buttons
    """
    StateFile = 'state/ui/buttoner.json'

    def __init__(self):
        super().__init__() # minimal setup
        os.makedirs('state/ui', exist_ok=True)
        if os.path.isfile( self.StateFile ):
            with open( self.StateFile, 'r') as fh:
                # [ {pin:x, topic:x}, ... ]
                self.assignments = json.load(fh)
        else:
            with open( self.StateFile, 'w') as fh:
                pass
            self.assignments = {}

        atexit.register(self.cleanup)
        GPIO.setmode(GPIO.BOARD)
        for assignment in self.assignments:
            self.setup_pin( **assignment )

    def setup_pin(self, pin, topic):
        GPIO.setup( pin, GPIO.IN, pull_up_down=GPIO.PUD_UP )
        debug(f"Add Pin {pin} -> {topic} IS {GPIO.input(pin)}")

        GPIO.add_event_detect(pin, GPIO.FALLING, bouncetime=50)

    def loop(self):
        for assignment in self.assignments:
            if GPIO.event_detected( assignment['pin'] ):
                debug(f"Button! {assignment}")
        time.sleep(0.01)

    def cleanup(self):
        GPIO.cleanup()

# RUN
x= UIButtoner( )
while(True):
    x.loop()

