"""
Use:
    from mqtt_local_config import connection as mqtt_local_config # from ./lib/

    async with mqtt.Client(
        **mqtt_local_config,
        ) as client:
"""

import paho.mqtt as paho

# paho mqtt args
connection = {
    "hostname" : "localhost",
    "port" : 1884,
    # client_id
    # tls...
    "clean_session" : True, # if a short enough down-time, resync to the commands
    "keepalive" : 8, # trigger Will
    "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
}
