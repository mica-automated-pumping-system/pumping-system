
See the ...schem.png, and ...bb.png for circuit diagrams.

See accel-stepper/ for the Arduino code.

## Overview

A 3V Arduino is level-shifted to 5V drive a chain of shift-registers. The shift-registers are 5V devices, as are the stepper-controllers. The stepper-motors are driven by typical TB6600 stepper-controllers (with opto-isolator inputs).

Shift-registers are used to allow the use of Arduino's with fewer pins (cf. the Mega and Grand-Central), and allow expansion to more motors (e.g. 15 motors have been run with the same architecture).

The software uses the AccelStepper library so that the motors can be accelerated/decelerated up/from speed. This gives smoother behavior for higher pumping rates. It also gives reliable/precise rates. The library is well proven. This software wraps the library in a shift-register adapter, since the AccelStepper library itself defaults to directly driving pins.

Some effort was made to "safe" the stepper-signals:

* The shift-registers `enable` is off (HIGH) by a pull-up. So, no spurious signals should go to the stepper-controllers during power-on. The `enable` is asserted (LOW) when the Arduino starts.
* The shift-registers have the `clear` signal asserted (LOW) by a pull-down. So, they should be 0'd on power-on. The `clear` signal is removed (HIGH) when the Arduino starts.

The outputs of each shift-register is in 2 groups of 4:
    stepper-0: Enable, Step, Direction, Unused. Outputs 1..4
    stepper-1: Enable, Step, Direction, Unused. Outputs 5..8
The shift-register nearest the Arduino controls motors 0 & 1.

**I think I reversed the sense of forward? Because our peristaltic pump setup wants to pump out as counter-clockwise. We somehow got stuck on pumping from the back of the box to the front.**

See accel-stepper/* for wiring info. Notably, the TPIC6C596 shift-registers are _sink_ devices, so they connect to the "-" on the stepper-controllers. Note that the "enable" signal is complement: off is enable. The stepper-controllers are opto-isolated, and each isolator can draw almost 20mA. 

The expectation is that the pumps are run at a relatively low-speed. But, the top speed on the ItsyBitsy M4 (or any SAMD51/M4) is about 10K steps/second: at 4 microsteps, that's about 450 RPM (about 7.5 revolutions/second). Sending status can interfere with the timing at high-speeds.

### Arduino command/Status interface

Movement is relative, since this is a pump. Status is reported as distance-to-go (i.e. counting down).

Try sending a "?" plus CR over the serial connection. You should get a summary of supported commands.

The central commands are Go, Stop, Get-Status, and Ping; and the Arduino will announce Status. Commands are terminated by CR.

e.g. `G 0 16.0 +200\n` is run pump 0, at 16.0 hz, forward 200 steps. A "-" pumps backwards.

e.g. `X\n` is stop-all-pumps-now.

The Arduino sends e.g. `m 0 100 5 1 40 60` as the distance-to-go for each pump:

* Whenever a pump reaches 0.
* Whenever an `m\n` is sent to request status.

## Customizing

Any Arduino compatible can be substituted for the ItsyBitsy, noting the requirements:

* Digital pins as noted in the .ino
* SPI
* Either a 3V Arduino wired through the level-shifter, or a 5V Arduino bypassing the level-shifter. Note the intentional correspondance of the Arduino pin numbers to level-shift pin numbers (e.g. A5 to A5) for simplicity.
* Fast enough, with enough RAM.

It is intended that the Arduino be dedicated to running the stepper-motors. At low speeds (typical of most pumping), this does leave lots of spare time to run other activities. At high speeds, the speed of calculations for the motors would be affected.

### Extra pins: Arduino, level-shifting, and shift-register

* Many pins are unused on the Arduino.
* 3 extra level-shifting pins are available. (A6,A7,A8). In the perfboard design it is intended that an extra screw-block serves as the header for them. Thus, for example, some pins directly from the Raspberry Pi could be level-shifted. Note that the level-shift is bi-directional.


The circuit design 
number of pumps

## Warnings

The stepper-controllers were designed to prevent noise from being sent back towards the Arduino, thus the opto-isolators. And, thus, the power for the motors (typically 12 to 48 volts) should be a completely separate circuit from the Arduino and other devices. Specifically, do not connect the motor-power ground to the Arduino ground.

## Testing

* accel-stepper "command line". Try "?"
* other .ino's for testing.

