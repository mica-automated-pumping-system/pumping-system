# (cd $thisdir; make ...)
# try: make help
# the list of services is a manual list in the variable `services`. Update that as you go.

MAKEFLAGS += --no-builtin-rules

systemd_unit_dir:=~/.config/systemd/user
# temporarily disable units (mask)
systemd_disable:=$(shell for s in node-red; do if [ -e $$s ]; then echo $$s; fi; done)
# nb: we assume pwd=systemd.units

.PHONY: status
# systemd, and cli status (dev relevant)
status :
	cd .. && tools/systemd-status-helper $(services)

.PHONY: install
# make, install & start systemd services services as user (i.e. on target system)
install : user-pre from-templates user-cp user-start /etc/logrotate.d/pump-pi

.PHONY: dev
# make, but don't start user-systemd-services (marked 'disable'). for dev/manual use
dev : user-pre from-templates

extant_agents:=$(shell cd .. && /bin/ls -1 agents/*.py)
extant_from_agents:=$(addsuffix .service,$(shell echo $(extant_agents) | sed 's/\//__/g'))
extant_templates:=$(shell /bin/ls -1 *.template | grep -v pythonscript.service.template)
extant_from_templates:=$(basename $(extant_templates))
extant_mqtt_action_config:=$(shell cd .. && /bin/ls -1 config/agents/mqtt-action-*.json)
extant_mqtt_actions:=$(addsuffix .service, $(basename $(notdir $(extant_mqtt_action_config))))
extant_mqtt_loggers:=$(addsuffix .service, $(basename $(notdir $(shell cd .. && /bin/ls -1 config/agents/mqtt-logger-* 2>/dev/null ))))
from_templates:=$(extant_from_agents) $(extant_from_templates) $(extant_mqtt_actions) $(extant_mqtt_loggers)
bin_services:=$(filter-out $(from_templates),$(shell /bin/ls -1 *.service))
services:=$(filter %.service,$(from_templates) $(bin_services))

.PHONY : from-templates
# create final files from .template, and other specials. see --help
from-templates : $(from_templates)
	@ echo "## From templates"
	@ ls $^

.PHONY : clean
clean : remove
	rm $(from_templates) || true

.PHONY : ddebug
ddebug :
	ls ../agents

% : %.fixup
	cp $< $@ # auto rm'd
	escpwd=`pwd | xargs dirname | sed 's/\//\\\\\//g'`; \
	sed -i -e "s/WORKINGDIR/$$escpwd/g" -e "s/XUSER/$$USER/g" $@
	if [ `systemctl --version | head -n 1 | awk '{print $$2}'` -lt 240 ]; then \
		sed -i -e "s/=append:/=file:/" $@; \
	fi;
	echo "## From Fixup $@ --->>> $<"

# Both tools/agent_*.py and agents/*.py
# doesn't need dependency on the ../agents/*.py or ../tools/agent_py
# log dir is always log/agents/
%.py.service.fixup : pythonscript.service.template
	cp pythonscript.service.template $@
	d=`echo $@ | sed "s/__.\+//"`; \
	b=`basename $@ .service.fixup | sed "s/.\+__//"`; \
	sed -i -e "s/AGENTDIR/$$d/g" -e "s/AGENT/$$b/g" $@; \
	mkdir -p ../log/agents; \
	mkdir -p ../state/$$d; \
	mkdir -p ../config/$$d; \
	echo "## Fixup: $@"

mqtt-logger-%.service.fixup : ../config/agents/mqtt-logger-% pythonscript.service.template
	# the config file is 2 lines: port, list of topics
	echo $@ : $<
	cp pythonscript.service.template $@;
	escpwd=`pwd | xargs dirname | sed 's/\//\\\\\//g'`; \
	d=agents; \
	mkdir -p ../log/$$d; \
	mkdir -p ../state/$$d; \
	mkdir -p ../config/$$d
	@# slightly hackish to decide on which mqtt service we use
	if [ `head -n 1 $<` = "1885" ]; then sed -i -e "s/mqtt-broker\.service/mqtt-broker-public.service/g" $@; fi
	sed -i -e "s/AGENTDIR/agents/g" -e "s/AGENT/config-$(notdir $<)/g" $@
	sed -i -e "/^Description=/ c \Description=mqtt-logger using $<" $@
	# nb: not config/agents/*, but the config for this service
	ms=`which mosquitto_sub` sed -i -e "/^ExecStart=/ c \ExecStart=`( read port; read topics; echo -n $$(which mosquitto_sub) "-F '%%I q%%q r%%r %%t %%p' -p $$port "; for t in  $$topics; do echo -n "-t" "'$$t' "; done; echo ) < $<`" $@

#OK
mqtt-action-%.service.fixup : ../config/agents/mqtt-action-%.json pythonscript.service.template
	echo $@ : $<
	echo "# Special case, mqtt-action-*, if config/agents/mqtt-ation*, from pythonscript.service.template" > $@
	cat pythonscript.service.template >> $@;
	b=`basename $<`; \
	sed -i -e "s/AGENTDIR/agents/g" -e "s/AGENT/$$b/g" $@;
	sed -i -e "/^Description=/ c \Description=Run the local agent $< using tools/agent-mqtt-action.py" $@
	# nb: not config/agents/*, but the config for this service
	perl -i -np -e '/^ExecStart=/ && do { my $$x="$<"; $$x =~ s/^\.\.//; s/=.*/=WORKINGDIR\/tools\/agent-mqtt-action.py WORKINGDIR$$x/;};' $@;
	d=agents; \
	mkdir -p ../log/$$d; \
	mkdir -p ../state/$$d; \
	mkdir -p ../config/$$d

# The default is just from the template
%.fixup : %.template
	cp $< $@

.PHONY : list-services
# List the services that we try to make/install (`services` variable)
list-services :
	@ echo "Templates: " $(extant_from_templates)
	@ echo "Agents: " $(extant_agents)
	@ echo "mqtt-action: " $(extant_mqtt_actions)
	@ echo "mqtt-logger: " $(extant_mqtt_loggers)
	@ echo "Bin services: "
	@ echo $(bin_services) | xargs -n 1 echo "  " | sort
	@ echo "Services: "
	@ echo $(services) | xargs -n 1 echo "  " | sort

.PHONY: show
# show the systemd units in --user
show: status

.PHONY: help
# list likely top-level targets
help: _help list-services

.PHONY: _help
_help:
	@ echo 'USAGE:'
	@ awk 'FNR==1,/^$$/ {print}' Makefile
	@ perl -ne 'use v5.18; our $$phony; our @comments; /^\.PHONY\s*:\s+([\w-]+)/ && do {chomp; @comments=(), $$phony=$$1;}; /^#/ && do {chomp; push @comments, $$_;}; /^[\w-]+\s*:/ && do {if ($$phony && @comments) { say "$$phony","\n\t",join("\n\t",@comments); $$phony=undef; @comments=();}};' $(firstword $(MAKEFILE_LIST))
	@ escpwd=`pwd | xargs dirname | sed 's/\//\\\\\//g'`; echo "WORKINGDIR: $$escpwd"

.PHONY: user-pre
user-pre: $(systemd_unit_dir)
	systemctl --user stop $(services) & true

.PHONY: user-cp
user-cp:
	@# Actually, it's a hard ln to the file so editing is less confusing
	for origunit in $(services); do \
		if [ -e $$origunit ]; then \
			rm $(systemd_unit_dir)/$$origunit 2>/dev/null || true; \
			ln `pwd`/$$origunit $(systemd_unit_dir); \
		fi \
	done
	for installed in $(systemd_disable); do \
		systemctl --user disable $$installed; \
	done;
	systemctl --user daemon-reload

.PHONY: user-start
user-start:
	# no `reload` should be needed after an `enable`
	systemctl --user enable $(services);
	systemctl --user start $(services)

# Install the logrotate.d (sudo)
/etc/logrotate.d/pump-pi : etc_logrotate.d_pump-pi
	sudo -p '#%# cp $< $@; su-password:' cp $< $@
	sudo chmod a+r $@

.PHONY: start
# starts the services, but does not `enable` them. so not persistent across reboot
start:
	systemctl --user start $(services)

.PHONY: stop
# stops the services, but does not `disenable` them. so not persistent across reboot
stop:
	systemctl --user stop $(services)

.PHONY: user-disable
user-disable:
	# no `reload` should be needed after an `disable`
	systemctl --user disable $(services)

.PHONY: remove
# Deletes the unit files from systemd
remove:
	# no `reload` should be needed after an `disable`
	systemctl --user stop $(services) || true
	systemctl --user disable $(services) || true
	rm $(addprefix $(systemd_unit_dir)/, $(services)) || true
	systemctl --user daemon-reload

~/.config/systemd/user :
	mkdir -p $@
	chmod 760 $@

define markdown_topics_output =
awk '/# Listening:/,0==1 {print}' | awk '/# (Listening|Publishing)/ {printf "\n'$$prefix'#%s\n\n",$$_; next}; {printf "    %s\n",$$_}'
endef

	@prefix=$${TOPICPREFIX:-#} ai=0;\
	cd ..; \
	for agent in $(extant_agents); do \
		ai=$$(( $$ai + 1 )); \
		echo "$$prefix#" $$ai. $$agent; \
		env NOGPIO=1 $$agent --help 2>/dev/null | \
		perl -n -e 'if (/^$$/ ... /^options:/) { print if ! /^options:/ }'; echo; \
		env NOGPIO=1 $$agent --topics 2>/dev/null | \
		$(markdown_topics_output); \
		echo; \
	done

.PHONY: document-topics
# List the agents and their topics
document-topics :
	@prefix=$${TOPICPREFIX:-#} ai=0;\
	cd ..; \
	for agent in $(extant_agents); do \
		ai=$$(( $$ai + 1 )); \
		echo "$$prefix#" $$ai. $$agent; \
		env NOGPIO=1 $$agent --help 2>/dev/null | \
		perl -n -e 'if (/^$$/ ... /^options:/) { print if ! /^options:/ }'; echo; \
		env NOGPIO=1 $$agent --topics 2>/dev/null | \
		awk '/# Listening:/,0==1 {print}' | awk '/# (Listening|Publishing)/ {printf "\n'$$prefix'#%s\n\n",$$_; next}; {printf "    %s\n",$$_}'; \
		echo; \
	done

.PHONY: document-action-topics
document-action-topics :
	@# For tools/agent-mqtt-action.py where config/agents/mqtt-action-*.json
	@# This is almost the same as 'document-action', but factoring gnu-make is ugly
	@prefix=$${TOPICPREFIX:-#} ai=0;\
	cd ..; \
	for action_config in $(extant_mqtt_action_config); do \
		ai=$$(( $$ai + 1 )); \
		service=systemd.units/$$(basename $$action_config .json).service; \
		echo "$$prefix#" $$ai. $$action_config; \
		true echo $$action_config "->" $$service; \
		true ls -l $$service; \
		cmd=$$(grep '^ExecStart=' $$service | awk -F= '{print $$2}'); \
		env NOGPIO=1 $$cmd --topics | \
		grep -v '# Publishing:' | \
		$(markdown_topics_output); \
	done


.PHONY: update-readme
# Update the ../README.md (i.e. with document-topics)
update-readme:
	@cd ..; \
	mv ../README.md ../README.md.bak; \
	perl -n -e 'my $$prefix;' \
		-e 'if (/^(#+) Minus80 Agents/) {' \
		-e 	'$$prefix=substr($$1,0); print; print "\n";' \
		-e '};' \
		-e 'if (/^(#+) Minus80 Agents/ .. /()(END Minus80 Agents)/) {' \
		-e	'if ($$1) { # Beginning of range' \
		-e 		'# Regular agents' \
		-e		'system("(cd systemd.units; env TOPICPREFIX=$$prefix make -s document-topics)");' \
		-e 		'# Action agents' \
		-e		'print("\n$$prefix# ACTIONS via tools/agent-mqtt-action.py\n\n");' \
		-e		'system("(cd systemd.units; env TOPICPREFIX=$$prefix make -s document-action-topics)");' \
		-e	'}' \
		-e	'# Hack for end of range' \
		-e 	'if ($$2) {print};' \
		-e	'# Dont print within range' \
		-e 	'next; ' \
		-e '};' \
		-e 'if (/END Minus80 Agents/) {print;next}' \
		-e 'print;' \
		../README.md.bak > ../README.md

# disable implicit
.SUFFIXES:
