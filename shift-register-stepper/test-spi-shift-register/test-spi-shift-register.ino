/*
  Has several test modes in loop().
  For writing shift-registers.
  Assumes:
  A chain of out shift-registers (serial-to-parallel) on spi MOSI,

  Testing:
    adjust the MOTOR_CT to get the REGISTER_CT you want

    Probably do in this order:
    set SLOW=1 & SLOWLATCH=1
      more output, blinks led-bar, and long delay between shift in/out
    spi-read
      same, shows spi works at much higher speed
    shift-write
      put leds on various out-bits
      most likely to succeed: shows out-wiring is ok
    spi-write
      shows spi works at much higher speed
    set SLOWLATCH=0
    spi-write
      shows higher speed is ok for in-latch
    set SLOW=0 & hookup a stepper to various out-shift-registers
    spi-write
      shows it running steppers at more realistic speed

*/
#include <Streaming.h>

#include <SPI.h>
#include <array_size.h>

#include "every.h"

// set to true to go slow (500ms between transfer), and printout values: easily seen blink
#define SLOW 1
// set to true for large delay between latch/etc
#define SLOWLATCH 1

constexpr int OUTCHIPS = 4;
constexpr int BITS_PER_MOTOR = 4;
constexpr int used_bits = 3; // can ignore other bits-per-motor, out sets them to high

constexpr int MOTOR_CT = OUTCHIPS * 8 / BITS_PER_MOTOR; // nb: see REGISTER_CT, i.e. to get bytes divide by bits-per
constexpr int REGISTER_CT = (int) ceil((float)MOTOR_CT * BITS_PER_MOTOR / 8);

struct init_pin_struct {
  // use me for pins that are like global-enable, etc
  const int pin;
  const boolean setup_to; // change to this when setup is ready
};

// Pick/define your pin setup
// set the #define to your setup
//#define AWG_ITSYM0TRINKET 1
#define AMPSPI_PERFBOARD
//#define AWG_ITSYM0TRINKET 1
//#define FRANKENAMP1_ITSYM4 1
#if defined AWG_ITSYM0TRINKET
constexpr int latch_pin = 1;
const init_pin_struct pin_setup[] = {};
#elif defined FRANKENAMP1_ITSYM4
constexpr int latch_pin = 7;
const init_pin_struct setup[] = {};
#elif defined AMPSPI_PERFBOARD
// itsy m4
const init_pin_struct sr_enable = { A5, LOW };
const init_pin_struct sr_clear = { A4, HIGH };
const init_pin_struct pin_setup[] = { sr_enable, sr_clear };
constexpr int latch_pin = A3;
#endif

constexpr int LATCHSTART = HIGH;
constexpr int LATCHIDLE = ! LATCHSTART;
//constexpr int SPISELECT = 4; // samd21's don't need this, and we aren't doing selects

constexpr byte used_mask = (1 << used_bits) - 1;
constexpr byte frame_mask = (1 << BITS_PER_MOTOR) - 1;

byte bit_vector[ REGISTER_CT ];
byte sentinel = 0x69;
constexpr int MAX_BYTE_I = array_size(bit_vector) - 1;

void dump_bit_vector(byte *bytes, int byte_ct = 0);// arduino does not like default args in definition
void dump_byte(byte b);

void setup() {
  clear_bright_leds();

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(latch_pin, LATCHIDLE);

  // other pins (if any)
  for ( init_pin_struct an_pin_setup : pin_setup ) {
    pinMode(an_pin_setup.pin, OUTPUT);
    digitalWrite(an_pin_setup.pin, an_pin_setup.setup_to);
  }

  // not actually used by us
  //pinMode(SPISELECT, OUTPUT);
  //digitalWrite(SPISELECT, LOW); //release chip

  Every fastblink(100);
  Timer serialtimeout(1500);
  Serial.begin(115200); while (!Serial & !serialtimeout()) digitalWrite(LED_BUILTIN, ! digitalRead(LED_BUILTIN));
  Serial << endl << F("Begin motors ") << (8 * array_size(bit_vector) / BITS_PER_MOTOR)
         << F(", bytes ") << array_size(bit_vector)
         << F(", bits per ") << BITS_PER_MOTOR
         << F(" SLOW? ") << SLOW
         << endl;

  for (unsigned int i = 0; i < array_size(bit_vector); i++) {
    bit_vector[i] = 0xff;
  }
}

void loop() {
  // pick one

  //shift_write();
  spi_blink( ) ; // blink based on input
  //spi_write();

  //spi_leakage_test(); // clean shifting? use with jig

  //spi_wiring_test(); // use this one w/test-jig (spi)

  // runs 1 pin in a 500ms blink, to isolate signal lines
  blink_pin(
    // comment out all but the one of interest:

    -2 // do nothing
    //-1 // just blink LED_BUILTIN
    //latch_pin
    //SCK
    // // non pass through:
    //MOSI
    //MISO
  );
}

void blink_pin(int pin) {
  static boolean first_time = true;
  if (pin == -2) return;

  if (first_time) {
    if (pin != -1) {
      pinMode(pin, OUTPUT);
      digitalWrite(pin, LOW);
    }
    pinMode(LED_BUILTIN, OUTPUT);
    first_time = false;
  }

  static Every::Toggle blink(500);
  if (blink() ) {
    if (pin != -1) {
      digitalWrite(pin, blink.state);
    }
    digitalWrite(LED_BUILTIN, blink.state);

    Serial << F("Blink ") << pin << F(" ") << millis() << endl;
  }
}

void shift_write() {
  static int ct  = 0;
  static boolean first_time = true;
  if (first_time) {
    // aka setup()
    pinMode(MOSI, OUTPUT);
    pinMode(SCK, OUTPUT);
    pinMode(latch_pin, OUTPUT);
    digitalWrite(latch_pin, LOW);
    first_time = false;
  }

  static Every say(500);
  boolean say_now = say();

  static Every::Toggle rapid_blink(500); // best if odd multiple of say's time
  boolean rapid_blink_now = rapid_blink();
  byte out_bits;
  boolean rapid = true; // just "blink" vs not?

  if (rapid) {
    // off used-bits, on unused-bits
    out_bits = (~0 & used_mask)  | (~0 & (frame_mask ^ used_mask));
    if (rapid_blink.state) out_bits = (0 & used_mask)  | (~0 & (frame_mask ^ used_mask));
    else out_bits = (~0 & used_mask)  | (0 & (frame_mask ^ used_mask));
    digitalWrite(LED_BUILTIN, rapid_blink.state);
    if (rapid_blink_now) {
      //Serial << F("rapid one byte ") << _BIN(out_bits); Serial << endl;
    }
  }
  else {
    // off used-bits, on unused-bits
    out_bits = (0 & used_mask)  | (0 & (frame_mask ^ used_mask));
    digitalWrite(LED_BUILTIN, HIGH);
  }
  if (say_now) {
    Serial << endl;
    Serial << F(" frame ") << ct << F("  "); dump_byte(out_bits);
    Serial << F(" ") << millis() << endl;
  }

  byte out_byte = fill_bit_vector( out_bits, bit_vector);

  //if (rapid_blink_now) { Serial << F("out bits "); dump_bit_vector(bit_vector, array_size(bit_vector)); }

  if (say_now) {
    Serial << F("one byte "); dump_byte(out_byte); Serial << endl;
    Serial << F("out bits "); dump_bit_vector(bit_vector, array_size(bit_vector));
  }

  for (unsigned int i = 0; i < array_size(bit_vector); i++) {
    shiftOut(MOSI, SCK, MSBFIRST, bit_vector[i]);
    //Serial << F("shifted ") << i << F(" "); dump_byte(out_byte); Serial << endl;
  }
  digitalWrite(latch_pin, LATCHSTART);
  delay(SLOW ? 300 : 10);
  digitalWrite(latch_pin, LATCHIDLE);
  delay(SLOWLATCH ? 200 : 10);
  ct += 1;
}

byte fill_bit_vector( byte out_bits, byte bit_vector[ REGISTER_CT ]) {
  // build a full byte
  byte out_byte = 0;
  for (int i = 0; i < 8 / BITS_PER_MOTOR; i++) {
    out_byte |= out_bits << i * BITS_PER_MOTOR;
  }
  // build bit_vector
  for (unsigned int i = 0; i < REGISTER_CT; i++) {
    bit_vector[i] = out_byte;
  }

  return out_byte;
}

void spi_blink() {
  // blink bits to test outputs
  // Console input:
  //    0,1,2..5 which motor
  //    e,s,d,x (or a,b,c,d) which bit
  // and all others are opposite bit (so blink against it)
  // motor[0] is first shift-register in chain, outputs 0,1,2,3.

  static int motor_i = 0;
  static int esdx = 0; // which bit to blink 0,1,2,3

  static byte pattern[MOTOR_CT] = {0, 0, 0, 0, 0, 0}; // just low-nybble for each motor, assemble later

  static byte shift_pattern[MOTOR_CT / 2]; // FIXME bit/motor stuff
  static boolean first_time = true;
  static boolean recalc = true;

  if (first_time) {
    first_time = false;
    pinMode(latch_pin, OUTPUT);
    SPI.begin();
    first_time = false;
    Serial << "mosi " << MOSI
           << " sck " << SCK
           << " latch " << latch_pin
           << endl;
  }
  if (recalc) {
    recalc = false;

    for (int i = 0; i < MOTOR_CT; i++) {
      pattern[i] = 0;
    }

    pattern[motor_i] = 1 << esdx; // ha, just which bit

    // NB: LSB-first, and [0] first. which makes it furthest
    // so, reverse byte order
    for (int i = 0; i < MOTOR_CT / 2; i++) {
      shift_pattern[MOTOR_CT / 2 - i - 1] =  pattern[i * 2] | (pattern[i * 2 + 1]  << 4);
    }
    Serial << F("Blink bits, far-->near "); dump_bit_vector(shift_pattern, MOTOR_CT / 2);
    digitalWrite(LED_BUILTIN, LOW); // to match
  }

  if (Serial.available() > 0) {
    boolean changed = true;
    char cmd = Serial.read();
    switch (cmd)  {
      case 'e':
      case 'a':
        esdx = 0;
        break;
      case 's':
      case 'b':
        esdx = 1;
        break;
      case 'd':
      case 'c':
        esdx = 2;
        break;
      case 'x':
        esdx = 3;
        break;

      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
        motor_i = cmd - '0';
        break;

      default :
        Serial << F("Nope ") << cmd << endl;
        changed = false;
        break;
    }
    if (changed) {
      Serial << F("Motor[") << motor_i << F("][") << esdx << F("]") << endl;
      recalc = true;

      Serial << F("Blink bits, far-->near "); dump_bit_vector(pattern, MOTOR_CT / 2);
      Serial << endl;
      digitalWrite(LED_BUILTIN, LOW); // to match

      return; // to cause recalc before writing
    }
  }


  x_spi_blink(shift_pattern, array_size(shift_pattern));
}

void spi_write() {
  // sets up a stepper-motor sequence to step 1 step forward per loop.
  // dir before step
  // writes out (the 2nd stage of stepping): actual move & led-bar
  // (does the 3rd stage of stepping): pulse off

  // bits: enable, step,dir
  // Need to set the dir w/o changing step
  // Three for very first step, and final step
  //  the extra cleanup step costs about 2% time
  // cw
  //static const byte pattern[] = { 0b100, 0b110, 0b100 }; // en|step|dir bits
  // ccw
  static const byte pattern[] = { 0b101, 0b111, 0b101}; // en|step|dir bits
  x_spi_step(pattern, array_size(pattern));
}

void x_spi_blink( byte  pattern[], const int pattern_ct) {
  // given the pattern, writes, ~, repeat
  // blinks BUILTIN as reference: Matches the bits you initially set (if you start with led-builtin LOW)

  static boolean first_time = true;
  if (first_time) {
    pinMode(latch_pin, OUTPUT);
    SPI.begin();
    first_time = false;
    Serial << "mosi " << MOSI
           << " sck " << SCK
           << " latch " << latch_pin
           << endl;
  }

  static Every check_speed(1000);
  static int ct = 0;

  if ( check_speed() ) {
    Serial << F("hz ") << ct / 800.0 << endl;
    ct = 0;
    if (!SLOW) delay(1); // time for usb interrupt and stepper speed
  }
  ct += 1;

  //Serial << F("msb ") << _HEX(bit_vector[ 0 ]) << F(" next ") << alt << endl;

  //digitalWrite(LED_BUILTIN, HIGH);


  digitalWrite(latch_pin, LATCHIDLE);

  // spi write whacks the source pattern, so copy
  memcpy(bit_vector, pattern, pattern_ct); // gaa, unsafe bit_vector[ct]!=pattern_ct

  // bit_vector[ MAX_BYTE_I ] = pattern[i]; // while debugging byte order
  if (SLOW) {
    // Serial << F("lsb ") << _BIN(bit_vector[ 0 ]) << endl;
    Serial << F("out bits "); dump_bit_vector(bit_vector, array_size(bit_vector));
  }

  // MSB first puts bit0 in output0
  // 20m is max (probably, depends on microchip, shift registers are somethin like 20ns)
  // 2m reduces fastest loop from 16.94hz to 10.48 hz, about .62 as fast,
  SPI.beginTransaction(SPISettings(SLOW ? 1000000 : 2000000, MSBFIRST, SPI_MODE0));
  // sends 0...[n-1] [n], no matter what MSB/LSB first is!
  // so, nearest shift-register is [n]
  SPI.transfer(bit_vector, array_size(bit_vector)); // whacks bit_vector
  SPI.endTransaction();

  if (SLOW) delay(300);// comment to test speed, uncomment to see blinkin
  digitalWrite(latch_pin, LATCHSTART);
  digitalWrite(LED_BUILTIN, ! digitalRead(LED_BUILTIN));

  if (SLOWLATCH) delay(200);// comment to test speed, uncomment to see blinkin


  if (SLOW) {
    //Serial << F("--- bytes ") << (array_size(bit_vector)) << F(" sentinel 0x") << _HEX(sentinel) << endl;
  }
  if (!SLOW) {
    // max loop speed is ~17Hz, about 1016 rpm
    delayMicroseconds( 1000000.0 * 1 / (3 * 800.0)); // hz. >3 seems to stall, would need accel, higher amps, etc
  }

  // blink (i.e. reverse all bits)
  for (int i = 0; i < pattern_ct; i++) {
    pattern[i] = ~ pattern[i];
  }
}

void x_spi_step(const byte  pattern[], const int pattern_ct) {
  // sets up a stepper-motor sequence to step 1 step forward per loop.
  // dir before step
  // writes out (the 2nd stage of stepping): actual move & led-bar
  // (does the 3rd stage of stepping): pulse off

  static boolean first_time = true;
  if (first_time) {
    pinMode(latch_pin, OUTPUT);
    SPI.begin();
    first_time = false;
    Serial << "mosi " << MOSI
           << " sck " << SCK
           << " latch " << latch_pin
           << endl;
  }

  static Every check_speed(1000);
  static int ct = 0;

  if ( check_speed() ) {
    Serial << F("hz ") << ct / 800.0 << endl;
    ct = 0;
    if (!SLOW) delay(1); // time for usb interrupt and stepper speed
  }
  ct += 1;

  //Serial << F("msb ") << _HEX(bit_vector[ 0 ]) << F(" next ") << alt << endl;
  digitalWrite(LED_BUILTIN, HIGH);

  digitalWrite(LED_BUILTIN, HIGH);
  //for (byte p : pattern) {
  for (int i = 0; i < pattern_ct; i++) {
    digitalWrite(latch_pin, LATCHIDLE);
    fill_bit_vector(pattern[i], bit_vector);

    // bit_vector[ MAX_BYTE_I ] = pattern[i]; // while debugging byte order
    if (SLOW) {
      // Serial << F("lsb ") << _BIN(bit_vector[ 0 ]) << endl;
      Serial << F("out bits "); dump_bit_vector(bit_vector, array_size(bit_vector));
    }

    // 20m is max (probably, depends on microchip, shift registers are somethin like 20ns)
    // 2m reduces fastest loop from 16.94hz to 10.48 hz, about .62 as fast,
    SPI.beginTransaction(SPISettings(SLOW ? 1000000 : 2000000, LSBFIRST, SPI_MODE0));
    // sends 0...[n-1] [n], no matter what MSB/LSB first is!
    // so, nearest shift-register is [n]
    SPI.transfer(bit_vector, array_size(bit_vector)); // whacks bit_vector
    SPI.endTransaction();

    if (SLOW) delay(300);// comment to test speed, uncomment to see blinkin
    digitalWrite(latch_pin, LATCHSTART);
    if (SLOWLATCH) delay(200);// comment to test speed, uncomment to see blinkin
  }
  digitalWrite(LED_BUILTIN, LOW);

  if (SLOW) {
    Serial << F("--- bytes ") << (array_size(bit_vector)) << F(" sentinel 0x") << _HEX(sentinel) << endl;
  }
  if (!SLOW) {
    // max loop speed is ~17Hz, about 1016 rpm
    delayMicroseconds( 1000000.0 * 1 / (3 * 800.0)); // hz. >3 seems to stall, would need accel, higher amps, etc
  }
}

void dump_bit_vector(byte *bytes, int byte_ct) {
  // NB; [8] is shifted out last, so is shift-register nearest ard
  // We reorder here so it reads right-to-left
  if (byte_ct == 0) byte_ct = array_size(bit_vector); // optional count

  for (int bi = 0; bi < byte_ct; bi++) {
    dump_byte(bytes[bi]);
    Serial << "  ";
  }
  Serial << endl;
}

void dump_byte(byte b) {
  for (int bit_i = 0; bit_i < 8; bit_i++) {
    if ( ! (bit_i % BITS_PER_MOTOR) && bit_i != 0) Serial << ".";
    Serial << ( ( b & (1 << (7 - bit_i)) ) ? '1' : '0' );
  }

}
