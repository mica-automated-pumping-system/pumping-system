import inspect,sys,os
from datetime import datetime

def debug(msg,ddata=None,up=0,level=0):
    # if lambda...
   
    limit_level = int(os.getenv('DEBUG','0'))
    if level <= limit_level:
        frame = inspect.currentframe()
        for i in range(0,up+1):
            poss_frame = frame.f_back
            if poss_frame: # prevent too far
                frame = poss_frame

        frame_code = frame.f_code
        fname = frame_code.co_filename
        command_dir = os.path.dirname( os.path.realpath(sys.argv[0]) )
        if fname.startswith(command_dir):
            # trim app dir, esp for in a macro
            fname = fname[ len(command_dir)+1 : len(fname)]
        sys.stderr.write( f"[{datetime.now().isoformat(timespec='seconds')} {fname}:{frame.f_lineno}] {msg} {ddata if ddata!=None else ''}\n" )
    return ddata
