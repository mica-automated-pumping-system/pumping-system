
async def stopper(a_step):
    # we are concurrent w/paus
    await asyncio.sleep(0.05) # let pause start
    await a_step.lib_agent.mqtt_client.publish( a_step.lib_agent.topic_for_stop(), '' )

recipe = repeat('Top Repeat {}', 2,
    parallel('2nd parallel',
        sequential('Seq 1',
            comment('Com 1'),
            repeat('Inner repeat', 2,
                patch_recipestep_call(pause(0.2), stopper, concurrent=True) # stops other pause too
            )
        ),
        sequential('Seq 1',
            comment('Com 2'),
            pause(0.25),
        ),
    )
)

