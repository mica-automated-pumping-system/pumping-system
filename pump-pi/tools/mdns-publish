#!/usr/bin/env python3
"""
NB: the system python3, because avahi/dbus is painful.

Like avahi-publish, with can specify ipv4, ipv6 or both

If you see appending "-2", try `sudo service avahi-daemon restart`
"""

import sys,os
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )

import argparse,re

import _dbus_bindings
import dbus

# requires that the avahi/__init__.py is available for python3, default install is only for python2.7 or system's python3
# i.e.: is python3 compatible: https://github.com/lathiat/avahi/tree/master/avahi-python/avahi
# use python3|python2 -v -c 'import avahi' |& less # to find paths
# then copy to our ./lib
import avahi 

from time import sleep

class AvahiDbus:
    # we are a singleton
    # to share `server` and `group`, for multiple
    def __new__(cls):
        if not hasattr(cls, 'instance'):
          cls.instance = super(AvahiDbus, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        bus = dbus.SystemBus()
        self.server = dbus.Interface(bus.get_object(avahi.DBUS_NAME, avahi.DBUS_PATH_SERVER), avahi.DBUS_INTERFACE_SERVER)

    def available(self):
        """Check if the connection to Avahi is still available."""

        try:
            self.server.GetVersionString()
        except dbus.exceptions.DBusException as e:  # ...don't really care, just checking.
            log.debug("Avahi is unavailable: %s", e.get_dbus_name())
            return False

        return True

class AvahiPublish:
    def __init__(self, iface, proto, name, service, port, txt):
        # `proto` can be "all", "ipv4" or "ipv6"
        if proto == "ipv4":
            self.proto = avahi.PROTO_INET
        elif proto == "ipv6":
            self.proto = avahi.PROTO_INET6
        elif proto == "all":
            self.proto = avahi.PROTO_UNSPEC
        else:
            raise ValueError(f"`proto` must be one of 'all', 'ipv4', 'ipv6', saw {proto}")

        server = AvahiDbus().server
        bus = dbus.SystemBus()
        self.group = dbus.Interface(bus.get_object(avahi.DBUS_NAME, server.EntryGroupNew()),
                               avahi.DBUS_INTERFACE_ENTRY_GROUP)

        self._service_name = name
        index = 1
        while True:
            try:
                self.group.AddService(
                    iface, # an index, try: ip address. avahi.IF_UNSPEC, # ? how to specify an iface
                    self.proto, 
                    0, # ? 
                    self._service_name, service, 
                    '', '', # domain, host
                    port, 
                    avahi.string_array_to_txt_array(txt)
                )
            except dbus.DBusException: # name collision -> rename
                index += 1
                self._service_name = '%s #%s' % (name, str(index))
            else:
                break

        self.group.Commit()

    def get_service_name(self):
        return self._service_name

    def unpublish(self):
        self.group.Reset()
        # FIXME: and now we are not published. flag? allow publish again?

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('name', help="service name, used for lookup like a host-name, unique", type=str)
    parser.add_argument('type', help="service type, like _http._tcp", type=str)
    parser.add_argument('port', help="service's port", type=int)
    parser.add_argument('txt', help="k=v e.g. for mqtt: info=the/description/topic", nargs='*', type=str)
    parser.add_argument('--ipv4','-4', help="publish for ipv4 only (default=--all)", action='store_true')
    parser.add_argument('--ipv6','-6', help="publish for ipv6 only (default=--all)", action='store_true')
    #parser.add_argument('--ipall', help="publish for ipv4 & ipv6 (the default)", action='store_true')
    parser.add_argument('--iface', help="short-name or index (`ip address`), default is all", type=str)
    args = parser.parse_args()

    firstwait=True
    while not AvahiDbus().available():
        if firstwait:
            firstwait = False
            sys.stdout.write("Avahi not ready")
        sys.stdout.write('.')
        sys.stdout.flush()
        sleep(3)

    if args.ipv4:
        proto = 'ipv4'
    elif args.ipv6:
        proto = 'ipv6'
    #elif args.ipall:
    #    proto = 'all'
    else:
        proto = 'all'
   
    if args.iface != None:
        if re.match('^\d+$', args.iface):
            iface = int(args.iface)
        else:
            sys_iface = f"/sys/class/net/{args.iface}/ifindex"
            if os.path.exists( sys_iface ):
                with open(sys_iface,'r') as fh:
                    iface = int(fh.read())
            else:
                raise ValueError("No such interface (by name). `--iface` requires a index or short name, try: ip address")

    else:
        iface = avahi.IF_UNSPEC

    announcer = AvahiPublish(iface, proto, args.name, args.type, args.port, args.txt)
    print( announcer.get_service_name() )

    while True:
        sleep(60)
