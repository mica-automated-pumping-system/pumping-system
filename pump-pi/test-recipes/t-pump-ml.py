# Run ml-stepper with:
# env DEBUGSHORT=1 agents/ml-stepper.py

recipe = sequential("Set & pump test",
    steps_per_ml(1, 86.0),
    pump_ml(1, 200, 100)
)
