
async def stopper(a_step):
    # ends up stopping before the comment is sent (and before progress is sent!)
    # which is fine for this test
    await a_step.lib_agent.mqtt_client.publish( a_step.lib_agent.topic_for_stop(), '' )
    await asyncio.sleep(0.1) # mqtt round-trip
    debug(f"stopper actions done")

recipe = patch_recipestep_call( comment("Comment"), stopper )
