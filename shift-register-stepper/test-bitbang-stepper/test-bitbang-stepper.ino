/*
  Bit bang 3 pins to figure out polarity
  Serial commands
*/

#include <Streaming.h>

constexpr int EnPin=10;
constexpr int DirPin=11;
constexpr int StepPin=12;

boolean en = LOW;
boolean dir = LOW;
boolean step = LOW;

void setup() {
  Serial.begin(115200);
  while (!Serial) delay(10);
  Serial.println(F("Start " __FILE__ " " __DATE__ " " __TIME__));

  pinMode(EnPin, OUTPUT);
  pinMode(DirPin, OUTPUT);
  pinMode(StepPin, OUTPUT);
}

void loop() {
  static boolean stepping = false;
  static boolean fast = false;

  if (stepping) {
    Serial << step;
    digitalWrite(StepPin, step);
    delayMicroseconds(fast ? 1 : 1000*1000);
    Serial << F(" ") << (!step) << endl;
    digitalWrite(StepPin, !step);
    delay(fast ? 10 : 2000);
  }
  
  if (Serial.available() > 0) {
    char x = Serial.read();
    if (x != '\n') {
  
      switch (x) {
        case 'E':
        case 'e':
          en = x=='E';
          break;

        case 'S':
        case 's':
          step = x=='S'; // S for High-to-low step, 's' for low-to-high step
          break;

        case 'F':
        case 'f':
          fast = (x=='F');
          break;


        case 'g':
          stepping = !stepping;
          break;
      }
      digitalWrite(EnPin, en);
      Serial << F("En ") << en << F(" Dir ") << dir << F(" Step ") << step 
        << F(" STEPPING ") << stepping
      << endl;
    }
  }
  delay(10);
}
