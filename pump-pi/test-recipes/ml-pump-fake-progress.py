#!/usr/bin/env python3.11
# env SCRIPT=xxx [SENDSTOP=1] ---

"""To help test the lib/recipe_dsl_maps.py pump_ml step:
    A recipe that waits for a ml_stepper start command,
    sends several updates
    possibly sends a /stop to run-dsl-recipe
    and finishes.
    env SENDSTOP=1 will send a /stop at 60%
"""

runner_topic = 'local/commands/maps-dsl-executor' # + /pump-ml-1-aio.py/stop'
pump_ml_topic = 'local/commands/pumps-ml/pumpbox1'
pump_ml_event = 'local/events/pumps-ml/pumpbox1'

# We aren't fast enough to subscribe to local/commands/maps-dsl-executor/#(retain=0) to capture the script-name, and then setup for the next command
# So, ENV
state = {'script' : os.getenv('SCRIPT') } # avoid `global`

class waitfor_pump_ml_start(waitfor):
    async def __call__(self):
        mqtt_message = await super().__call__()
        # we have to adapt to the pump-n, and know the ML
        # we will ignore the rate!
        state['pump_n'] = int( str(mqtt_message.topic).split('/')[-1] )
        state['ml_payload'] = mqtt_message.payload
        debug(f"Start pump-ml: {state}")

class send_progress(send):
    """send a percent of state['ml_payload']. 1.00=done"""
    def __init__(self, percent, *args, **kwargs):
        # we update topic & payload at call time
        super().__init__(*args, topic=None, payload={}, **kwargs)
        self.percent=percent

    async def __call__(self):
        # update payload
        self.topic = f"{pump_ml_event}/{state['pump_n']}"
        self.payload = {"pump": state['pump_n'], "steps": 1408, "ml": state['ml_payload']['ml'] * self.percent }
        if self.percent==1.00:
             self.payload['finished'] = True

        debug(f"Send {self.topic} : {self.payload}")
        await super().__call__()

        if os.getenv('SENDSTOP',None) and self.percent >= 0.6:
            await self.lib_agent.mqtt_client.publish( f"{runner_topic}/{state['script']}/stop" )

recipe = sequential("Fake pumps-ml interaction",
    waitfor_pump_ml_start( "Wait for pump start", pump_ml_topic + "/1", {} ), # any command to "pump_ml", store the result
    send_progress( 0.30 ),
    send_progress( 0.60 ),
    #send_progress( 1.00 )
)
