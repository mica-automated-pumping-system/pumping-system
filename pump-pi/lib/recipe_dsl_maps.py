"""Lab automation dsl + MAPS specific dsl steps"""
from recipe_dsl import *
import json

# FIXME: these should be introspection somehow
pump_ml_topic = 'local/commands/pumps-ml/pumpbox1'
pump_ml_event_topic = 'local/events/pumps-ml/pumpbox1'
pump_ml_stop_topic = 'local/commands/pumps-ml/pumpbox1/stop' # FIXME: standard pattern

async def pump_ml_message( _at, _suffix=None, **payload ):
    topic = pump_ml_topic

    if _suffix:
        topic += _suffix

    debug(f"> [{_at}] {topic} -> {payload}")
    await RecipeStep.lib_agent.mqtt_client.publish( topic, json.dumps(payload), retain=False, qos=1 )

async def pump_ml_wait( _at, _duration, progress_callback=None, **payload ):
    # sends the payload, awaits the response /event/, with timeout
    # returns the response
    # can throw timeout
    # FIXME: should have a callback for progress=...
    try:
        return await RecipeStep.lib_agent.send_and_waitfor(
            send = f"{pump_ml_topic}/{payload['pump']}", 
            payload=payload,
            progress_callback=progress_callback,
            response=f"{pump_ml_event_topic}/{payload['pump']}", 
            matching={ 'finished':True },
            timeout = _duration * 1.1 + 1,
        )
    # let cancel bubble up
    except asyncio.TimeoutError as e:
        await comment("Timeout during pump_ml", _at)()
        raise e

class steps_per_ml(RecipeStep):
    def __init__(self, pump_n, steps_per_ml, up=0, at=0):
        super().__init__(duration=0.001, up=up+1, at=at)
        self.pump_n=pump_n
        self.steps_per_ml=steps_per_ml
        self._state = False
        self._resume = True
        self.base_progress_data = { 'pump_n' : self.pump_n, 'steps_per_ml':self.steps_per_ml }

    def state(self):
        # we want to ensure we sent the steps_per_ml, and duplicates are harmless
        return [self.__class__.__name__, {'done':self._state}]
    
    def resume(self, step_name, rest, done):
        # see comment in state()
        self._resume = not done

    async def __call__(self):
        await super().__call__()
        self._state = False
        await self.say_progress(f"set steps_per_ml {self.pump_n} {self.steps_per_ml}/ml")
        if self._resume:
            await pump_ml_message( self.at, _suffix='/steps_per_ml',  pump=self.pump_n, value=self.steps_per_ml )
        self._state = True
        self._resume = True

class pump_ml(RecipeStep):
    """
    Can throw asyncio.TimeoutError in __call__
    """
    def __init__(self, pump_n, ml, rate, why=None, at=0, up=0 ):
        super().__init__(up=up+1, at=at)
        self.duration = abs(ml) / rate if rate !=0 else 0
        self.pump_n=pump_n
        self.ml=ml
        self.ml_sofar = None
        self.rate=rate
        self.why=why
        self.start=0
        self._resume = 0
        self._last_consumed_secs = None
        self.base_progress_data = { 'why':self.why, 'pump_n' : self.pump_n, 'duration' : self.duration, 'ml' : self.ml }

        self.totals = {'secs': self.duration, 'ml' : self.ml }

    def human_id(self):
        sofar = str(self.ml_sofar) if self.ml_sofar != None else ''
        return f"{self.human_id_prefix}{self.at}[{self.pump_n}:{sofar}/{self.ml}@{self.rate}]"

    def state(self):
        return [self.__class__.__name__, {'progress':self.ml_sofar}]

    def resume(self, step_name, rest, progress):
        self._resume = progress

    def progress_callback(self, mqtt_message):
        now = time.monotonic()

        consume_ml = mqtt_message.payload['ml'] - self.ml_sofar;
        self.ml_sofar = mqtt_message.payload['ml']
        #debug(f"# PROGRESS {self.ml_sofar} / {self.ml}")
        elapsed = now - self.start

        consume_secs = now - self._last_consumed_secs if elapsed <= self.duration else 0
        self._last_consumed_secs = now

        progress_data = { 'sofar':self.ml_sofar, 'elapsed':elapsed, 'consume_ml':consume_ml, 'consume_secs':consume_secs }
        asyncio.create_task( self.say_progress(f"pump-ml {self.why or ''} {self.pump_n} {self.ml}ml {self.rate} ml/sec {h_duration(self.duration)}",progress_data=progress_data) )

    async def __call__(self):
        self.ml_sofar = 0 # before we get first update
        try:
            self.start = time.monotonic()
            await super().__call__()
            self.ml_sofar = 0
            self._last_consumed_secs = self.start

            progress_data = { 'sofar':self.ml_sofar, 'elapsed':0, 'consume_ml':self.ml_sofar }
            await self.say_progress(f"pump-ml {self.why or ''} {self.pump_n} {self.ml}ml {self.rate} ml/sec {h_duration(self.duration)}",progress_data=progress_data)

            the_rest_ml = 0
            the_rest_secs = 0
            elapsed = 0
            if abs(self.ml) - abs(self._resume) > 0.0:
                # FIXME: the first local/events/pumps-ml/pumpbox1/1 will have the actual ml for the actual steps
                #   {"pump": 1, "steps": 1408, "ml": 16.368001023000062}
                # display that at end
                # FIXME: should have a `progress` function too, saying how far along we are
                # {"pump": 3, "steps": 13763, "ml": 8.0393864}
                pump_ml_event = await pump_ml_wait( 
                    self.at, self.duration, 
                    progress_callback=self.progress_callback,
                    pump=self.pump_n, ml=self.ml - self._resume, rate=self.rate
                )
                the_rest_ml = pump_ml_event.payload['ml'] - self.ml_sofar
                self.ml_sofar = pump_ml_event.payload['ml']

                now = time.monotonic()
                elapsed = now - self.start
                the_rest_secs = self.duration - elapsed
                if the_rest_secs < 0:
                    the_rest_secs = 0
            self._resume = 0
            progress_data = { 'sofar':self.ml_sofar, 'elapsed':elapsed, 'consume_ml': the_rest_ml, 'consume_secs':the_rest_secs }
            await self.say_progress(f"pump-ml done {self.why or ''} {self.pump_n} {self.ml_sofar:0.4f}/{self.ml}ml ({h_duration(time.monotonic()-self.start)}/{h_duration(self.duration)}): ",progress_data=progress_data)

        except asyncio.TimeoutError as e:
            progress_data = { 'sofar':self.ml_sofar, 'elapsed':time.monotonic()-self.start }
            await self.say_progress(f"pump-ml timeout {self.why or ''} {self.pump_n} ~{self.ml_sofar:0.4f}/{self.ml}ml ({h_duration(time.monotonic()-self.start)}/{h_duration(self.duration)})", progress_data=progress_data)
            # FIXME: do our 'stopped' logic to capture state
            raise asyncio.CancelledError("pump_ml timeout")

        except asyncio.CancelledError as e:
            # FIXME: get current pump_ml
            # stop pump_ml
            await RecipeStep.lib_agent.mqtt_client.publish( pump_ml_stop_topic )
            progress_data = { 'sofar':self.ml_sofar, 'elapsed':time.monotonic()-self.start }
            await self.say_progress(f"pump-ml stopped {self.why or ''} {self.pump_n} ~{self.ml_sofar:0.4f}/{self.ml}ml ({h_duration(time.monotonic()-self.start)}/{h_duration(self.duration)})", progress_data=progress_data)
            # FIXME: for recovery, remaining ml
            raise e
