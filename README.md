# AMPS Automated Pumping System

A laboratory peristaltic pumping system, using MQTT.

Can be made using laser-cutter. Of course, requires much hardware.

Currently fixed at 6 peristaltic pump heads, but fairly easy to adjust the number (see [issues](https://gitlab.com/mica-automated-pumping-system/pumping-system/-/issues)).

Notably, can pump in or out.

Runs recipes composed of out, in, pause, repeat. With each head being able to run an independant sequence.

Interface is through a node-red dashboard (GUI), and possible external MQTT (IOT).

## Architecture

The control system is split into parts:

1. Any computer running a browser, wifi capable, and supporting zeroconf/bonjour. This is the GUI interface.
2. A networkable logic controller (Raspberry Pi). The Pi runs several micro-services that communicate over MQTT, possibly communicates to internet server(s) over MQTT, controls the Arduino over USB, and runs the local GUI. See the [pump-pi/](https://gitlab.com/mica-automated-pumping-system/pumping-system/-/tree/pump-pi/pump-pi) directory.
3. A real-time sub-controller (Arduino) for steppers, running the AccelStepper library and limited serial communication. See the [shift-register-stepper/](https://gitlab.com/mica-automated-pumping-system/pumping-system/-/tree/pump-pi/shift-register-stepper) directory for diagrams, schematics, software.
4. An internet server that supports MQTT and some kind of dashboard, for remote access.

A 3V ItsyBitsy was chosen as the real-time stepper controller. With the use of shift-registers, a large number of GPIO pins isn't required. But, we use several pins for some enable signals, and SPI. Size of the Arduino isn't important, but the ItsyBitsy is cheap and available. As the number of motors increases, the higher SPI speed may become relevant. Only a fraction of the Arduino RAM is used.

The stepper-motors are driven by typical TB6600 stepper-controllers. The controllers are driven by an Aruino+shift-register sytem that can be expanded to many steppers. See the [shift-register-stepper/](https://gitlab.com/mica-automated-pumping-system/pumping-system/-/tree/pump-pi/shift-register-stepper) directory for diagrams, schematics, software.

## Hardware

Laser cut acrylic box, stepper motor driven peristaltic pump heads, Raspberry Pi and Arduino controllers, stepper-controllers, etc. DIN rail mountings.

See the [hardware]() directory for instructions, bill-of-materials, etc.
