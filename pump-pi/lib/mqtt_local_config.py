"""
Get the `connection` from config/mqtt-local-config.py,
"""

config_file = 'config/mqtt-local-config.py'

with open(config_file, mode='r') as fh:
    # FIXME: feedback on any exception
    code = fh.read()
    # FIXME: feedback on any exception
    compiled_code = compile( code, config_file, mode='exec')
    code = None

script_globals = {}
exec( compiled_code, script_globals )
connection = script_globals['connection']
print(f"## config {config_file} -> {connection}")


