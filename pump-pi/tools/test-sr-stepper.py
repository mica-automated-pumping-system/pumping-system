#!/usr/bin/env python3.11
"""
Test using serial port to run the shift-register-stepper on an arduino
--- rate steps ...
"""

import time,re
import sys,os
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )

import serial

from arduino_port import ArduinoPort
from every.every import Every

do_a_command = Every(0.25) # try a command a lot

start = time.time()
attempts = 0
successes = 0

commands = [
]
cmd_i=0;

# connect to arduino usb serial
MaxResultLength = 10 # chars, for readline() timeout (+ EOL)
MaxCommandLength = 10 # chars, for write() timeout (+ EOL) FIXME: n-chars * number of steppers
ResponseTimeout = 0.1 # arduino should never take longer than that to respond
serialport = ArduinoPort(
    timeout= 10.0/115200 * (MaxResultLength+1) + ResponseTimeout,
    write_timeout = 10.0/115200 * (MaxCommandLength+1),
    ) # uses pyserial

class RemoteStepper:
    # the remote should not volunteer any info

    def __init__(self, serial):
        self.check_ping = Every(0.3)
        self.live = False # has seen ping recently
        self.serial = serial
        self.check_status = Every(0.1)

        while ( self.serial.port.in_waiting > 0 ):
            sys.stdout.write( self.serial.port.read() )
        if self.ping(print_echo=True):
            self.live = True
        else:
            print("No ping yet")

    def heartbeat(self):
        """check for alive"""
        if self.check_ping():
            if self.ping(print_echo=True):
                self.live = True
            else:
                self.live = False
                print("No ping")

    def ping(self, print_echo=False):
        """Get an echo. print stuff if print_echo==True
        """
        try:
            self.serial.write('#')
        except serial.serialutil.SerialTimeoutException as e:
            return False
        except serial.serialutil.SerialException as e:
            if 'write failed' in str(e):
                self.serial.reconnect()
                return False
            else:
                raise e
        except Exception as e:
            print(f"E: {e.__class__}")
            raise e

        for _i in range(3):
            try:
                response = self.serial.readline()
            except serial.serialutil.SerialException as e:
                if 'device reports readiness' in str(e):
                    return False
                else:
                    raise e

            if b"#\r\012" == response:
                return True
            else:
                if print_echo:
                    print(f"> (waiting {self.serial.port.in_waiting}): {response}")
        return False

    def go(self, hz,steps):
        command = f"G{'+' if steps>=0 else '-'}{hz:04}{abs(steps):04}"
        print(command)
        self.serial.write(command)

    def status(self):
        """Get status
        """
        if (not self.check_status()):
            return 9999

        try:
            self.serial.write('?')
        except serial.serialutil.SerialTimeoutException as e:
            return 9999
        except serial.serialutil.SerialException as e:
            if 'write failed' in str(e):
                self.serial.reconnect()
                print(e)
                return 9999
            else:
                raise e
        except Exception as e:
            print(f"E: {e.__class__}")
            raise e

        for _i in range(3):
            try:
                response = self.serial.readline().decode('utf8', 'strict')
                m = re.match(r'\?-?(\d+)', response)
                #print(f"Stat {response} : {m}")
                if m:
                    t = int(m.group(1))
                    return t
                else:
                    print(f"> (waiting {self.serial.port.in_waiting}): {response}")
            except serial.serialutil.SerialException as e:
                if 'device reports readiness' in str(e):
                    return 9999
                else:
                    raise e


        return 9999



stepper = RemoteStepper(serialport)
print(f"Stepper live? {stepper.live}")

# get rid of command-name:
sys.argv.pop(0)

say_status = Every(0.5)

while(True):
    stepper.heartbeat()

    if (ss:=stepper.status()) == 0:
        if len(sys.argv)>= 2:
            print(f"go args {sys.argv }")
            stepper.go( int(sys.argv.pop(0)), int(sys.argv.pop(0)) )
    elif say_status():
        print(f"@ {ss}")

    time.sleep(0.20)

