
async def stopper(a_step):
    # we are concurrent w/pause()
    await asyncio.sleep(0.1) # let pause get started
    await a_step.lib_agent.mqtt_client.publish( a_step.lib_agent.topic_for_stop(), '' )

recipe = sequential("Seq Test",
    comment("Comment 1"),
    patch_recipestep_call( pause(1), stopper, concurrent=True )
)
