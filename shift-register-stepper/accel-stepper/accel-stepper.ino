/*
  A shift-register-accel-stepper controlled by commands on Serial.

  Very fast flash == No Serial connection (no recent incoming data).
  Slow flash == Serial connection.
  Assymetric slow flash (long on or long off) == Serial connection with pings
  Stuttering/Fast == Probably good serial + some commands (like pings/status)

  Wiring
  USB <-> Pi
  SPI SCK,MOSI -> shift-registers clock,data
  shift-register shift-register latch

  So, using an itsy m4
  PI              USB
  system GND      GND
  !(srclr)reset   A4  10k pull-up, we'll release when we start
  !(enable)       A3  10k pull-up, we'll release when we start
  !oe pull-low    NC
  clock -         sck
  l_clock -       A3
  ser_in          MOSI

  Shift-Register
  74h595
  SR nearest arduino is SR[0]
  outputs
    0,1,2,3 is motor[0]
    4,5,6,7 is motor[1]
    Signal order is: Enable, Step, Direction, Unused

  FIXME:
  Consider putting a pulldown on !RST on the shift registers.
    And then hooking to a "live" pin on the arduino
    So, clear outputs on power-up (esp enable),
    And arduino overrides it while live.
*/

#include <SPI.h>
#include <Streaming.h>
#include <every.h> // by awgrover

#include "array_size.h"

// Stuff for AccelStepperShift
// (74hc495 shift register: STEPPER_SIGNALS HIGH, ENABLE_POLARITY LOW, STEPPER_NORMAL_DIRECTION false)
// (TPIC6C596 (sink) shift register: STEPPER_SIGNALS LOW, ENABLE_POLARITY HIGH, STEPPER_NORMAL_DIRECTION true)
// signals on "+", common cathode
#define STEPPER_SIGNALS LOW
// Groups of 4 per shift-register, output1 == EN on shift-register.
#define SR_SIGNALS { SR_EN, SR_STEP, SR_DIR, SR_NC }
// TB6600 has Enable as No-current, Disable as current
#define ENABLE_POLARITY HIGH
// Step on falling edge
#define STEP_ON LOW
// our pump heads run CCW=forward
#define STEPPER_NORMAL_DIRECTION true
const int DISABLE_LIST[] = { -1}; /// { 5,6,9,11,0, -1 }; // 0..n, permanently not-use motors, end with -1

// Show timing for motor 0
// #define TIMING1
// print position of each motor on each step if true
// 0 = no step output
// 1 = step output on 100msec
// 2 = step output every step
// > 100 means print every n-msec
int DEBUGPOSPERSTEP = 0; // want changeable so processing can ask for steps
//#define DEBUGSTUPIDSLOW 100
// 2 to print on "blink"
// 4 to print all bit-vectors( step, dir)
//#define DEBUGLOGBITVECTOR 4
//#define DEBUGBITVECTOR 0

constexpr bool SLOWSPI = false; // Slow is 1mhz, otherwise 2mhz

#include "AccelStepperShift.h"
#include "Parsing.h"

struct init_pin_struct {
  // use me for pins that are like global-enable, etc
  const int pin;
  const boolean setup_to; // change to this when setup is ready
};

constexpr int LATCH_PIN = A3;
// setup pins: have pull-x's, in "safe" mode, that we change when we start
const init_pin_struct SR_ENABLE = { A5, LOW }; // low is enable, pull-up'd
const init_pin_struct SR_CLEAR_RELEASE = { A4, HIGH }; // low is clear, pull-down'd
const init_pin_struct PinSetup[] = { SR_ENABLE, SR_CLEAR_RELEASE };

constexpr int MOTOR_CT = 6; // FIXME: make settable?
//constexpr int BITS_PER_MOTOR = 4;

Every fast_flash(50);
// see comment in serial_commands()
bool SerialActive = false;

AccelStepperShift stepper_shift(
  MOTOR_CT,
  LATCH_PIN,
  DISABLE_LIST
);

void setup() {
  clear_bright_leds();

  static Timer min_startup_time(2000); // allow upload before loop()
  unsigned long start = millis();

  // Get serial started
  Serial.begin(115200); // will wait for serial below

  // SPI setup
  pinMode(LATCH_PIN, OUTPUT);
  digitalWrite(LATCH_PIN, AccelStepperShift::LATCHIDLE);
  // other pins (if any)
  for ( init_pin_struct a_pin : PinSetup ) {
    pinMode(a_pin.pin, OUTPUT);
    digitalWrite(a_pin.pin, a_pin.setup_to);
  }

  SPI.begin();

  // Wait for serial
  static Every serial_wait(2 * 1000); // how long before continuing. usu ~850, sometimes 1500
  pinMode(LED_BUILTIN, OUTPUT);

  // Wait for serial or timeout
  while (! (Serial || serial_wait()) ) {
    if (fast_flash()) digitalWrite( LED_BUILTIN, ! digitalRead(LED_BUILTIN) );
    delay(10); // the delay allows upload to interrupt
  }
  unsigned long took = millis();
  Serial.println(F("Start " __FILE__ " " __DATE__ " " __TIME__));
  Serial << F("Serial took ") << (took - start) << endl;

  // Allow time to upload before a bad loop() locks the arduino
  while ( ! min_startup_time() );

  stepper_shift.begin();
}

void loop() {
  static Every say_elapsed(1000);

  static Every check_serial(501);
  bool running = stepper_shift.run();

  if ( stepper_shift.did_one_finish ) {
    say_positions();
#ifdef TIMING1
    Serial << F("Elapsed ") << stepper_shift._timing_steps << F("/") << _FLOAT( (micros() - stepper_shift._timing_start) / 1000000.0, 6)
           << F(" ") << _FLOAT(stepper_shift._timing_steps / ((micros() - stepper_shift._timing_start) / 1000000.0), 4)
           << F(" hz")
           << endl;
#endif
  }

  serial_commands(! running && check_serial() );

  stepper_shift.finish_loop(); // let stepper cleanup for this loop

#ifdef TIMING0
  static unsigned loop_ct = 0;
  if (say_elapsed()) {
    Serial << loop_ct << endl; // hz
    loop_ct = 0;
  }
  loop_ct += 1;
#endif

}

bool test_serial(bool do_test) {
  bool rez = SerialActive;

  if (do_test) {
    // This is where you can do slow/disruptive things
    // without affecting stepping,
    // because we aren't stepping

    // FIXME: if interstep time > 1ms, we could do this anyway
    rez = !!Serial; // takes 1ms. see comment in serial_commands()
  }
  return rez;
}

void serial_commands(bool test_serial_this_time) {
  static Every say_stats(1000);
  //unsigned long start=micros();

  static Every heartbeat(800);

  // Testing for active serial connetion takes 1milli! So, we don't.
  // (Weirdly, Serial.available() takes with 1micro, or ~140micros, not 1milli, even if no Serial connection)
  // We just do evidence based.
  // Our client should do a heartbeat status/ping, which will trigger our evidence
  static Timer timeout(1500); // still active if we see any input before this timeout

  SerialActive = test_serial(test_serial_this_time);

  if ( SerialActive) {
    if (heartbeat() ) {
      // Serial << (!! Serial) << endl;
      digitalWrite( LED_BUILTIN, ! digitalRead( LED_BUILTIN ) );
    }
    else if (timeout()) {
      SerialActive = false;
    }
  }
  else if (fast_flash()) {
    digitalWrite( LED_BUILTIN, ! digitalRead(LED_BUILTIN) );
  }
  //unsigned long hb_time = micros();

  // So, we just blithely check for available
  if (Serial.available() > 0) {
    SerialActive = true;
    timeout.reset();

    //Serial << F("avail ") << Serial.available() << endl;
    char x = Serial.read();
    //Serial << F("... '") << x << F("'") << endl;

    // Responses are: data...\n

    // static Parsing::BaseClass * Commands[] = { new Parsing::Ping(), new Parsing::Go(), new Parsing::Discard(), NULL };
    // static Parsing::BaseClass** CurrentCommand = Commands;

    //static Parsing::Ping commands;
    //static Parsing::Discard commands;
    //static unsigned int value;
    //static Parsing::Entier<unsigned int> commands(F("motor"), value, 999, '\n');

    static Parsing::BaseClass * const debug_seq[] = {
      new Parsing::Char(F("d"), 'd'),
      new Parsing::OneOf(F("012"), "012", set_DEBUGPOSPERSTEP),
      &Parsing::eol
    };
    // gah, the list is const
    // "Commands Alternate"
    static boolean do_print_help = false;
    Parsing::PrintHelp print_help( &do_print_help );
    static Parsing::BaseClass * const commands_alt[] = {
      // approximate the order of command frequency
      // FIXME: "alt" prints long error message if anybody starts. should only error the committed path. also, getting error twice
      new Parsing::Ping(),
      new Parsing::SingleCommand(F("motor-positions"), 'm', say_positions ),
      new Parsing::GoCommand(stepper_shift),
      new Parsing::SingleCommand(F("stop"), 'X', stop_all ),
      new Parsing::Sequence(F("debug-perstep d0|1|2"), debug_seq, array_size(debug_seq) ),
      &print_help,
      
      new Parsing::Discard() // discards till eol w/o error. must be last
    };
    static Parsing::Alternate commands( F("commands"), commands_alt, array_size( commands_alt ) );

    //Serial << F("@ ") << commands.why << endl;

    if ( commands.consume(x) ) {
      //Serial << F("  Parsed ") << commands.why << F(" < '") << x << F("'") << endl;
      if ( commands.done ) {
        //Serial << F("### DONE PARSING ") << commands.why << F(" < '") << x << F("'") << endl;

        // real world feedback
        digitalWrite( LED_BUILTIN, ! digitalRead( LED_BUILTIN ) );

        // must be before .reset
        if (do_print_help) {
          print_help.print_help( commands_alt, array_size( commands_alt ) );
        }

        // it handled it
        commands.reset();
        //Serial << F("Top reset") << endl;

      }

      // else will continue parsing
    }

    else {
      if ( commands.error && ! commands.at_start ) {
        commands.say_error(x);
      }
      else {
        // not consumed, which shouldn't happen
        Serial << F("what? '") << x << F(" ") << endl;
        //if (commands.error) commands.say_error(x);
      }

      commands.reset();
    }
  }

  //unsigned long sbcheck_time =micros();
  if (say_stats()) {
    /*
      Serial << F("  SC total ") << (sbcheck_time-start) << endl;
      Serial << F("    hb ") << (hb_time-start) << endl;
      Serial << F("    check ") << (sbcheck_time-hb_time) << endl;
    */
  }
}

//// Callbacks (sometimes used outside of callback)

void say_positions() {
  Serial << 'm';
  for (int i = 0; i < stepper_shift.motor_ct; i++) {
    // will do "-" for neg direction
    Serial << F(" ") << stepper_shift.motors[i]->distanceToGo();
  }
  Serial << endl;
}

void stop_all() {
  for (int i = 0; i < stepper_shift.motor_ct; i++) {
    stepper_shift.motors[i]->setMaxSpeed( 0 );
    stepper_shift.motors[i]->move( 0 );
  }
  Serial << F("X") << endl;
}

void set_DEBUGPOSPERSTEP(const char x) {
  int val = x - '0';
  Serial << F("set DEBUGPOSPERSTEP=") << val << endl;
  DEBUGPOSPERSTEP = val;
}
