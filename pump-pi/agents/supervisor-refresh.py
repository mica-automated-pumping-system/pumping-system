#!/usr/bin/env python3.11

import asyncio
import sys,os
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
import paho.mqtt as paho

import agent
from simple_debug import debug

class SupervisorRefresh(agent.Agent):
    """
    We refresh the device list ~5secs
    """
    # config
    # pump-pi/mqtt.config
    # pump-pi/$device.config
    # ARGV config
    mqtt_config = {
        "device" : "supervisor",
        "name" : "refresh",
        "interval" : 5,

        "connection" : {
            "hostname" : "localhost",
            "port" : 1884,
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
        }
    }

    async def listener(self):
        # default for "get"
        await super().listener()

    async def loop(self, first_time):

        while(True):
            try:
                await asyncio.sleep( self.mqtt_config['interval'] )

                self.queue_a_message( 
                    self.topic_for_get( with_device=False, with_id=False),
                    ''
                )

            except KeyboardInterrupt:
                debug("!! ^C")

asyncio.run( SupervisorRefresh().run() )
