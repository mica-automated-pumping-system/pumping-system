#!/usr/bin/env python3.11
"""
Generate the widgets for each step of the standard recipe (see PumpRows below).
Hard-coded for "after" group "Init widget array", in grid "pump-row". See the `with flow(...` below

* Makes new_flow.json from current .node-red/projects/maps-pi/flow.json
* cp new_flow.json .node-red/projects/maps-pi/flow.json
* Then restart node-red

#   alias nrevert='git checkout HEAD .node-red/projects/maps-pi/flow.json'
#   nrevert; tools/nr-pump-tab && tools/flexdash-tool --flow new_flow.json hier pump-row; cp new_flow.json .node-red/projects/maps-pi/flow.json
"""


# build the graph
# post-fix-ups (rows, etc)
# write json

from dataclasses import dataclass,field
from typing import *
import re

import os,sys
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )

from simple_debug import debug
from flexdash import *

@dataclass
class PumpRow:
    name: str
    label: str
    fd_maker: Callable[..., None] # any of the fd-maker DSL methods
    args: list = field(default_factory=list)
    kwargs: dict = field(default_factory=dict)
    init_wire: str = 'init-pump-widget-array'

    def label_fixed(self):
        # flexdash's markdown is very incomplete
        # particularly: outline indents
        # so, change leading *'s to '. . '
        return re.sub('^\*+ ', lambda m: int(len(m.group(0))-1) * '.. ', self.label)

PumpRows = [
    #        row-name, label, fd_maker( [args], {kwargs}, init_wire='init-pump-widget-array' )
    # Note the default init-...-array wiring
    # The label gets the name {row_name}-label        
    # The widget gets the name {row_name}
    PumpRow( 'label', '# Step', markdown, [f"# x"], init_wire='init-pump-label-array' ),

    PumpRow( 'steps-per-ml', '**steps/ml**', stat, [], {'unit':'/ml'} ),

    # MIX
    PumpRow( 'cycles-repeat', '**cycles**', gauge ),

        PumpRow( 'cycle-start-ml', '* **cycles-start-ml**', sparkline ), # combined ml and ml/min
        PumpRow( 'cycle-start-in-pause', '* **cycle-pause**', gauge, [], {'unit':'secs'} ),

        PumpRow( 'increments-per-cycle-repeat', '* **incr/cycle**', gauge ), 
            PumpRow( 'increment-add-in-ml', '** **incr-add-ml**', sparkline ), # combined ml and ml/min
            PumpRow( 'increment-add-in-pause', '** **incr-add-pause**', gauge, [], {'unit':'secs'} ),

            PumpRow( 'loops-per-increment-repeat', '** **loops/incr**', gauge ),

                PumpRow( 'loop-out-ml', '*** **loop-out-ml**', sparkline ), # combined ml & ml/min
                PumpRow( 'loop-out-pause', '*** **loop-out-pause**', gauge, [], {'unit':'secs'} ),
                PumpRow( 'loop-in-ml', '*** **loop-in-ml**', sparkline ), # combined ml & ml/min
                PumpRow( 'loop-in-pause', '*** **loop-in-pause**', gauge, [], {'unit':'secs'} ),

            PumpRow( 'increment-end-pause', '* **incr-end-pause**', gauge, [], {'unit':'secs'} ),

        PumpRow( 'cycle-end-pause', '* **cycle-end-pause**', gauge, [], {'unit':'secs'} ),

    PumpRow( 'end-pause', '**end-pause**', gauge, [], {'unit':'secs'} ),

    # FILL
    PumpRow( 'fill-repeat', '**fills**', gauge ),
        PumpRow( 'fill-ml', '* **fill-ml**', sparkline ), # combined ml & ml/min
        PumpRow( 'fill-pause', '* **fill-pause**', gauge, [], {'unit':'secs'} ),
]

_nrpumptab_progress_wire = {} # function-name:i
def nrpumptab_progress_wire( newnode ):
    # figure out what `function` node to draw a wire from
    # We rely on the node-name to have patterns: blah-pause, blah-ml, blah-repeat
    # We rely on the `function` to have been already written, with the right number of outputs!
    # We rely on the `function` to be named w/pattern: x mix ml, x mix pause, x mix repeat

    debug(f"New Node")
    name = newnode['name']

    debug(f"  Name {name}")
    if m:=re.search(r'-(ml|repeat|pause)$', name):
        function_name = f"x mix {m.group(1)}"
        
        fport = _nrpumptab_progress_wire.get(function_name, 0)
        _nrpumptab_progress_wire[function_name] = fport+1

        wire(fport, function_name, newnode )
        debug(f"Progress wire: {function_name}[{fport}] -> {name}")
    else:
        debug(f"No progress wiring: {name}")

#debug(f"Rows {PumpRows}")
for r in PumpRows:
    print(f"{r.name:20} {r.label:30} -> {r.label_fixed()}")

with flow('Recipe Status'):
    with tab('run-status'):
        nr_flow.delete_by_name( 'pump-row-group', type='group') # get rid of it before we re-make it with `grid()`

        with grid('pump-row'):
            delete_all() # leaves the groups that are made for grids/panels
            # We need to delete the groups
            for old_group in ['label-col-group','pump-col-group']:
                group_ids = nr_flow.delete_by_name( old_group, type='group' )

            row_height = 2 # in panel "rows" (e.g. 2x)

            with layout(2,below='pump-widgets-below-here'):
                with default(fd_rows=row_height):

                    # Col: The "labels", markdown boxes
                    with panel('label-col', cols=2): # deduce rows & columns
                        with default(fd_cols=3):
                            for pump_row in PumpRows:
                                # label node, always markdown
                                markdown( pump_row.name+"-label", pump_row.label_fixed() )

                    # Col: the widgets (as 'array' style)
                    with panel('pump-col',cols=12):
                        with default(
                            fd_cols=4, 
                            fd_array=True,
                            fd_array_max=6,
                            gauge={'degrees':180},
                            stat={'iso-prefix':True},
                            ):

                            for pump_row in PumpRows:
                                # widget node
                                newnode = pump_row.fd_maker( pump_row.name, *pump_row.args, **pump_row.kwargs )

                                # do the init-array thing
                                wire(pump_row.init_wire, newnode )

                                # do the progress-wiring
                                if newnode['name'] not in [ 'steps-per-ml' ]: # don't "progress" wire these
                                    nrpumptab_progress_wire( newnode )

                    # ml-per-step is special
                    wire('x mix steps-per-ml', 'steps-per-ml')


"""
OBSOLETE, idea swamp

with tab('run-status'):
    * somebody has to reset the state to "not run yet"
    the-mqtt-step-status.wire_to(
        reform-topic-to-pump$n.wire_to(
            switch('switch-by-step', vs='payload.step_id')
        )
    )

    with grid('pump-row'):
        delete_all()

        with pump_ct(6):
            pump_row( '**steps/ml**', stat(), 'step1.' ) # recipe maker should add the "step id" to appear in progress message
            pump_row( '**cycles**', gauge(), 'step2.' )
            ...

def pump_row(label, display, stepid):
    # label | n * pump-display
    
    with node_layout_row():
        with panel('label-col'): # deduce rows & columns
            with default(cols=2):
                markdown( label, label ) # name, payload

        with panel('pump-col'):
            with default(
                cols=4, 
                array=True,
                gauge={'degrees':180},
                stat={'iso-prefix':True},
                ):
                re_ambient(display)

            # need to reform the payload? for each display widget?
            switch('switch-by-step').add_output( '==', stepid ).wire_to( display )
"""

fd_dump()
