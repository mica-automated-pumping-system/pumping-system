"""Progress and `finished` messages as we run recipes
"""
# q0 r0 local/events/maps-dsl-executor/aio-2023-03-02_11:08-recipe.py/162 {"#": "pause 00.0000 secs/00:01:00.00", "human_id": "aio-recipes/aio-2023-03-02_11:08-recipe.py.9[4|6].150[1/1].154[8/20].158[1/6].162[00.0000 secs/00:01:00.00]", "at": 162, "progress_data": {"pump_n": 4, "why": "loop-in", "duration": 60, "elapsed": 0}}

local_forward( 
    # which: our topic and matcher
    'local/events/maps-dsl-executor/+/+', 
    ( lambda message: debug(
        f"t? {message.payload}? ", 
        str(re.search(r'\.py/[^/]+$',message.topic))
        ) 
    # saw some lists here
    and isinstance(message.payload,dict) and re.search(r'\.py/[^/]+$', message.topic) and 'progress_data' in message.payload.keys() 
    ),

    # to: aio topic and payload
    './feeds/black-lightning.black-lightning-recipe-step', 
    (lambda original_message, p:
        {
        **p['progress_data'],
        'human_id' : p['human_id']
        }
    )
)

# local/events/maps-dsl-executor/aio-2023-03-02_11:08-recipe.py {"finished": true, "elapsed": "00.0048 secs"}
local_forward( 
    # which: our topic and matcher
    'local/events/maps-dsl-executor/+', 
    (lambda message: re.search(r'\.py$', message.topic) and message.payload), # any payload

    # to: aio topic and payload
    './feeds/black-lightning.black-lightning-recipe', 
    (lambda original_message, p: { original_message.topic.split('/')[-1] : p }), # the script name
    priority=True
)

