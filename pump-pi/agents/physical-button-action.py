#!/usr/bin/env python3.11

import asyncio
import sys,os,json,re
from random import choice
from copy import copy
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pip" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib" )
sys.path.append( os.path.dirname( sys.argv[0] ) + "/../lib-pump-pi" )
from simple_debug import debug
from rate_limiter import RateLimiter
from every.every import Every,Timer
import paho.mqtt as paho # some constants|--help'
try:
    import RPi.GPIO as GPIO
except ModuleNotFoundError as e:
    if not os.environ.get('NOGPIO',None):
        debug("Try: env NOGPIO=1 ...")
        raise e

import agent

class PinSetup:
    # NB: any parameters should be added to to_dict()
    def __init__(self, pin, invert=False, pull=1, event=-1, messages=[], rate_limit=None, keepalive=None, latch=None, as_boolean=False):
        # NB: any parameters should be added to to_dict()

        self.pin=int(pin) # is a key in the config, thus string
        self.pull=pull
        self.event=event
        self.messages=messages
        self.last_value = None # yet
        self.invert=invert # Use True for typical "closed switch pulls-down, with idle=pull-up"
        self.as_boolean = as_boolean

        self.rate_limit = None
        if rate_limit:
            self.rate_limit = RateLimiter( rate_limit['max'], rate_limit['secs'] )
            self.flapping = rate_limit.get('flapping', 10*rate_limit['secs'] )
            self.flapping_timer = Timer( self.flapping )

        self.keepalive = keepalive
        self.keepalive_timer = Timer(0.00001 if keepalive==None else keepalive) # may not get used
        self.keepalive_timer.start()

        self._latch = latch # to save to file
        self.latch = None
        if latch:
            self.latch = [False,True][ latch['dir'] ] # also, used as the flag: when != None
            self.latch_timer = Timer( latch['duration'] )
            debug(f"  latching active'ated to latch.dir: {self.latch}", level=2)


        # for env GPIOVALUES=
        self._igv_timer = Timer(0.00001) # may not get used
        #print(f"#### running? {self.pin} {self._igv_timer.running}")
        self._igv_timer.start() # so it expires first time
        #print(f"#### running? {self.pin} {self._igv_timer.running}")

    def setup(self):
        # True if we set it up
        if not os.environ.get('NOGPIO',None):
            x=[ GPIO.PUD_DOWN, None, GPIO.PUD_UP ]
            pud = x[self.pull + 1]
            debug(f"Pull {x} -> {pud}")
            try:
                if 'GPIO' in globals():
                    debug(f".setup {self.pin}")
                    GPIO.setup( self.pin, GPIO.IN, pull_up_down=pud )
            except ValueError as e:
                debug(f"ERROR: Not a valid pin: {pin}")
                return False

            debug(f"Add GPIO Pin {self.pin}")

            crossing = [ GPIO.FALLING, GPIO.BOTH, GPIO.RISING ]
            debug(f"crossing {crossing}")
            crossing = [ GPIO.FALLING, GPIO.BOTH, GPIO.RISING ][self.event + 1]
            debug(f"crossing -> {crossing}")
            if 'GPIO' in globals():
                GPIO.remove_event_detect(self.pin) # if someone else set it, or we are changing
                try:
                    debug(f"add event {self.pin}")
                    GPIO.add_event_detect(self.pin, crossing, bouncetime=100)
                except RuntimeError as e:
                    if str(e).startswith('Failed to add edge detection'):
                        sys.stderr.write(f"# The pin is not a valid pin (use board-numbering not 'gpio' number): {self.pin}\n")
                    raise e
        return True

    async def publish_on_event(self, mqtt_client, force=False):
        """ 
            force=True to read and publish current value 
        """
        debug(f"React to pin {self.pin}... {'using-gpio' if 'GPIO' in globals() else 'no GPIO'}")

        def _parse_env_gpio_value():
            # if debugging, or testing on non-pi (env NOGPIO=1 ...)
            # then use the provided `env GPIOVALUES=$pin:$v,$v,$v`
            #   where $v is 0|1
            #       0|1 are as-if read pin values
            #       +n.n is seconds of delay between, nb: '+' disambiguates 1|0 second
            # Returns the next value for this pin: True|False|float . T|F are pin values, float is delay
            if 'GPIOVALUES' not in globals():
                global GPIOVALUES

                env_val = os.environ.get('GPIOVALUES', None)
                GPIOVALUES={}
                if env_val != None:
                    # split into pin: v...,pin:, v...,pin: sets
                    p_m= re.split('(\d+):', env_val)
                    if len(p_m) < 2:
                        raise Exception(f"GPIOVALUES shoud =$pin:$v,...,$pin,$v.... Saw: {env_val}")
                    #print(f"### Splitted0 {p_m}")
                    p_m.pop(0) # extra ''
                    #print(f"### Splitted {p_m}")

                    # for the pairs
                    while p_m:
                        pin = p_m.pop(0)
                        values = p_m.pop(0).split(',')

                        if values[-1] == '':
                            # trailing '' if multiple pins in list
                            values.pop()
                        #print(f"### p {pin} : {values}")

                        GPIOVALUES[ int(pin) ] = [ (True if x=='1' else False) if x in ('0','1') else float(x)  for x in values ]

                debug( f"### GPIOVALUES parsed at {self.pin}:",GPIOVALUES )
                if GPIOVALUES and self.pin not in GPIOVALUES:
                    debug(f"  WARNING self.pin not in GPIOVALUES: [{self.pin}] GPIOVALUES {GPIOVALUES}")

        _parse_env_gpio_value() # need to setup GPIOVALUES global

        def _injected_gpio_value():
            global GPIOVALUES
            # (consume) next value
            igv = GPIOVALUES.get( self.pin, None )
            return igv.pop(0) if igv else None

        def _check_for_event():
            # returns hit bool, read-value fn
            # complexity because: `force` value, and testing via env GPIOVALUES=
            global GPIOVALUES

            # What event? maybe keepalive, maybe force, maybe testing with GPIOVALUE

            # Do keepalive send? even for `env GPIOVALUES=`
            if self.keepalive != None and self.keepalive_timer():
                debug(f"Maybe keepalive [{self.pin}] = {self.last_value}",level=4)
                if self.last_value != None:
                    debug(f"  use_last={self.last_value}", level=3)
                    hit = True
                else:
                    # this shouldn't happen (see "# special case setup" below)
                    debug(f"  (don't) use_last={self.last_value}")
                    hit = False

                read_fn = lambda: self.last_value

            # This whole branch of the if is for testing via:
            # env GPIOVALUES=...
            # (we will give no further pin-value changes once exhausted)
            #print(f"### IN env? {self.pin} {self.pin in GPIOVALUES} {GPIOVALUES}")
            elif self.pin in GPIOVALUES:

                #debug(f"  has GPIOVALUES: [{self.pin}] {GPIOVALUES[self.pin]}", level=3)
                #print(f"### running? {self._igv_timer.running}")
                if self._igv_timer() or force:
                    debug(f"  consume GPIOVALUES: [{self.pin}] {GPIOVALUES[self.pin]}", level=3)
                    # last delay expired (or force)
                    # next value...
                    if None != (igv:=_injected_gpio_value()):
                        debug(f"  use a GPIOVALUES: {igv} remaining {GPIOVALUES[self.pin]}", level=3)
                        if igv in (True,False):
                            # T|F pin values
                            debug(f"  :: inject {igv}", level=3)
                            hit = True
                            read_fn = lambda : igv
                            self._last_igv = igv # we may be `force'd`, which means we have to return some value, the last one

                            # have to set a zero-length delay
                            self._igv_timer.interval = ( 0.0001, 0 )
                            self._igv_timer.start()
                        else:
                            # start delay
                            debug(f"  :: delay {igv}", level=3)
                            self._igv_timer.interval = ( igv, 0 )
                            self._igv_timer.start()
                            hit, read_fn = (False, None)

                    # no more values
                    else:
                        hit, read_fn = (False, None)

                # still delaying...
                else:
                    hit, read_fn = (False,None)

                # we may be `force'd`, or rate-limit may expire, which means we have to return some value, the last one
                if read_fn == None:
                    if '_last_igv' not in dir(self):
                        raise Exception(f"Bummer, due to hackish code, you can't start GPIOVALUES with a delay, {sorted(dir(self))}")
                    read_fn = lambda: self._last_igv

            # GPIO test/read (maybe force)
            elif 'GPIO' in globals():
                debug(f"  use GPIO w/force={force}", level=3)
                read_fn = lambda: GPIO.input(self.pin)

                if force:
                    debug(f"  force", level=3)
                    hit = True

                # actual crossing-detect
                else:
                    try:
                        hit = GPIO.event_detected( self.pin )
                        if hit:
                            debug(f"  GPIO crossing", level=3)
                    except ValueError as e:
                        # we don't want to crash other pin actions if only this one is bad
                        debug(f"Not a valid pin apparently: {self}")
                        hit = False
                    
            # NOGPIO
            else:
                hit,read_fn = ( force, lambda : choice((True,False)) )
                if force:
                    debug(f"  NOGPIO random...")

            return (hit, read_fn)

        async def _publish(hit, read_fn):
            # something to send?
            if (
                # edge-detect
                hit
                # flapping expired, we need to publish
                or (self.rate_limit and self.flapping_timer())  
                ):

                debug(f"Event on pin {self.pin}", level=2)

                if self.latch != None:
                    # this has to be called to let it expire, even if rate-limited. we look at .running below
                    self.latch_timer() # not testing it here (see below: self.latch_timer.running), just letting it expire

                # If we didn't have the debug() in the rate limit block below
                # we could move the rate-limit block here, and avoid some code (don't need to read pin if rate-limited), etc.

                value = None
                if self.event == -1:
                    value = 0 # crossed to low
                elif self.event == 1:
                    value = 0 # crossed to high
                elif self.event == 0:
                    # we have to read the actual value if crossing==GPIO.BOTH (`event`==0)
                    value = GPIO.input(self.pin) if 'GPIO' in globals() else read_fn()
                else:
                    raise Exception(f"Internal: self.event should only be -1,0,1, saw: {self.event.__class__.__name__} {self.event}")
                def _invert_booleanize():
                    # so we can use in debug statements too
                    nonlocal value
                    # invert for sending as last thing
                    if self.invert:
                        debug(f"  inverting from {value} -> {not value}", level=2)
                        value = 0 if value==1 else 1
                    if self.as_boolean:
                        value = not not value

                # rate-limit? (should not rate-limit if flapping_timer expired because flapping_timer > rate-limit)
                # and, of course, don't rate limit if `force`
                if self.rate_limit and not force:
                    if not self.rate_limit.allowed():
                        _invert_booleanize() # for debugging, invert/booleanize
                        debug(f"Rate-limited! [{self.pin}] {'inverted to this:' if self.invert else 'actual'} {value} ")

                        if not self.flapping_timer.running:
                            self.flapping_timer.start()
                        return  # rate-limited!

                # latch'ing active?
                if self.latch != None:
                    debug(f"  latching active to latch.dir: {self.latch}", level=2)
                    if self.latch_timer.running:
                        # we ignore changes away from `dir`
                        debug(f"  latched from `{value}` -> latch.dir: {self.latch} (no send)", level=2)
                        # don't send during latching, "during latch" implies last sent value was latch.dir
                        # and we would merely send latch.dir this time
                        return 
                    else:
                        debug(f"  wasn't latching from `{value}` to latch.dir: {self.latch}", level=2)

                    if value == self.latch:
                        # in the future, we _will_ ignore changes away from `dir` till latch_timer() expires
                        # (alway restart if == )
                        if not self.latch_timer.running:
                            debug(f"  future latching to latch.dir `{self.latch}` because saw matching: {value}")
                        self.latch_timer.start()

                
                _invert_booleanize() # as needed

                # final value!
                debug(f"  [{self.pin}] -> {value}")

                # publish

                self.keepalive_timer.start() # don't need to keepalive because we are publishing now

                async with asyncio.TaskGroup() as tg:
                    for topic_payload in self.messages:
                        debug(f"## Send {topic_payload['topic']} {topic_payload.get('payload','')}", level=3)
                        payload = copy(topic_payload.get('payload',None))
                        if isinstance(payload,str):
                            payload = re.sub(r'\$value', str(value), payload)
                            payload = json.dumps(payload)
                        elif isinstance(payload,dict):
                            debug(f"  ## dict: {payload} <- v:{value.__class__.__name__} {value}", level=3)
                            if 'value' in payload:
                                payload['value'] = value
                            else:
                                if to_subst := next( (k for k,v in payload.items() if isinstance(v,str) and re.search(r'\$value',v) ), None ):
                                    payload[to_subst] = re.sub(r'\$value', str(value), payload[to_subst])
                                elif to_subst := next( (k for k,v in payload.items() if v==None), None ):
                                    payload[to_subst] = value
                            payload = json.dumps(payload)
                        elif payload == None:
                            payload = value
                        else:
                            # list, I guess
                            payload = json.dumps(payload)
                        debug(f"Payload: {payload.__class__.__name__} : {payload}")
                        tg.create_task( mqtt_client.publish( topic_payload['topic'], payload ) )
                debug(f"  update last_value={value}", level=4)
                self.last_value = value
                #debug(f"Sent topics")

        if force:
            # this branch is for the "get value" message event
            debug(f"  Force for get-value")
            await _publish( *_check_for_event() )
        else:
            while(True):
                # special case setup, for keepalive we need an initial read
                if self.keepalive != None and self.last_value == None:
                    debug(f"  Force for initial keepalive")
                    force = True

                await _publish( *_check_for_event() )
                force = False
                await asyncio.sleep(0.100) # try not to eat too much cpu, yet be responsive

    def cleanup(self):
        GPIO.remove_event_detect(self.pin)

    def __str__(self):
        return f"<PinSetup {self.pin} " + str(self.to_dict()) + ">"

    def to_dict(self):
        d = { "invert":self.invert, "pull":self.pull, "event":self.event, "keepalive":self.keepalive, "messages":self.messages, "latch" : self._latch, "as_boolean" : self.as_boolean }
        if self.rate_limit:
            d['rate_limit'] = { "max": self.rate_limit.max, "secs" : self.rate_limit.secs }
            if self.flapping != None:
                d['rate_limit']['flapping'] = self.flapping
        return d

class UIButtoner(agent.Agent):
    """
    This agent maps digital-pin transistions (e.g. switches) (on gpio-pins) to actions.
    When the pin-transition happens, we publish the topic with a payload.
    We use edge-detect (GPIO.add_event_detect), but we might miss events
        and we are still polling at 0.1 seconds
    See the configuration and defaults in `Example`.
    Add:
        local/commands/ui/buttoner/add/<pin> { "topic" : "a topic", payload={...} }
        local/commands/ui/buttoner/add/<pin>/<topic...> {payload}
        If you omit the payload, we will send {} FIXME?
        You can have more than 1 topic:payload on a pin.
            but, a duplicate topic:payload is only sent once.
    Remove
        local/commands/ui/buttoner/remove/<pin> { "topic" : "a topic", "payload" : {...} }
        local/commands/ui/buttoner/remove/<pin>/<topic> {payload}
        local/commands/ui/buttoner/remove/<pin> { "topic" : "a topic", "all":True} # removes all payloads for the topic
    List
        local/get/ui/buttoner/pins -> local/events/ui/buttoner/pins [ {pin:x, topic:x},...]
        local/get/ui/buttoner/<pin> -> "" or "topic"

    The assignments of pin->topic are saved, and loaded automatically on startup.
    config/agents/physical-button-action.json & state/agents/physical-button-action.json
    Example:
        {
            "#" : "You can put in comments using '#' as the key",
            # Each pin gets one pin-setup, and possibly many messages
            "18" : { # physical pin (`GPIO.BOARD`), not "GPIOn" (BCM)
                "#" : "You can put in comments using '#' as the key",
                "pull" : 0, # -1 for pull-down, 0 for none, 1 for pull-up. Default is 1: pull-up.

                "#" : "these parameters are in order of application:

                # Send on which edge-crossing. BEFORE invert
                # Note that for `0` we have to read the pin-value after the edge-detect,
                #   so it might not be what the edge-detect saw.
                # For -1|1 (fall|rise), we know the value, so don't have to read, 
                #   so it will be what edge-detect saw.
                "event" : 0, # -1 for falling, 0 for both, 1 for rising. Default -1: falling

                # send the last-value again, it it has been `keepalive` seconds since the last send
                # Nb: forces an initial read, and publish, of the gpio pin when we start
                # Nb: will be rate-limited
                "keepalive" : 60.0, # default is none: don't do keepalive

                # if rate-limit exceeded, will ignore transistions for `flapping` seconds, and then send current value
                # default is `null`: no rate-limit.
                "rate_limit" : { "max" : 1, "secs" : 10, "flapping" : 60 }, # `flapping` defaults to 10*secs

                # ignore transitions to the opposite of `dir` for `duration` seconds after a transition to `dir`
                # i.e. keep `dir` for at least `duration`. useful for oscillating values that go flatline on "off"
                # so, will not send a message if "during latch duration"!
                # nb: this also suppresses changes during the latch-duration
                #   e.g. given latching.dir=1, events 10101...latch-duration...10 
                #   will send a single True for the first 5 events,  then nothing, then a True for the last 2 events
                #   because the latch-duration started on the first `1`
                # you might want to combine with a rate-limit
                # `dir` is BEFORE invert'ing
                "latch" : { "dir" : 1, "duration" : 0.1 },  # null is default: no latching. dir=0|1

                # send the opposite of the read value. True for typical "closed switch shorts to ground, with pull-up"
                "invert" : false, # default is `false`: no invert

                "as-boolean" : false, # true will force values to `true`/`false`, default values are `1`/`0`

                # will substitute the pin value for $value, or {value:}
                "messages" : [ # default is no-messages. which seems useless.
                    { 
                        "topic": "local/devices/publisher/aio-expensive", 
                        "payload" : "$value" # none->button_value, $value gets substituted, {"value":x} x->button_value
                    }
                ]
            }
        }


    We use GPIO.BOARD numbering. So, it's the actual pin-number not the "gpio" number.

    example debug values
    env DEBUG=2 NOGPIO=1 GPIOVALUES=13:1,1,0,+0.5,0,0,1,0,0.3,0,0.3,0 agents/physical-button-action.py
    """
    mqtt_config = {
        "device" : "ui",
        "name" : "buttoner",

        "connection" : { # paho mqtt args
            "hostname" : "localhost",
            "port" : 1884,
            # client_id
            # tls...
            "clean_session" : True, # if a short enough down-time, resync to the commands
            "keepalive" : 8, # trigger Will
            "clean_start" : paho.client.MQTT_CLEAN_START_FIRST_ONLY,
        }
    }

    def __init__(self):
        super().__init__() # minimal setup
        if 'GPIO' in globals():
            # tolerate no gpio on non-pi for development
            debug(f"Set BOARD numbering")
            GPIO.setmode(GPIO.BOARD)

        self.assignments = {} # key:PinSetup() # read from our config and state

        # FIXME? should config always override the last state? i.e. predefined buttons
        changed = self.read_pin_assignment( self.config_file() )
        changed |= self.read_pin_assignment( self.state_file() )
        debug(f"Setup {self.assignments}")

        # We rewrite the state file
        # which captures config setup.
        if changed:
            self.save_state()

    def read_pin_assignment(self, pin_assignment_file ):
        # read them from a file
        # return true for changed

        changed = False

        debug(f"Read {pin_assignment_file}")
        if os.path.isfile( pin_assignment_file ):
            with open( pin_assignment_file, 'r') as fh:
                # {$pin: [ { topic:x, ...}, ...] }
                try:
                    pin_assignments = json.load(fh)
                except json.decoder.JSONDecodeError as e:
                    if 'Expecting value: line 1 column 1 (char 0)' in str(e):
                        # ok, empty file, move along
                        pin_assignments = []
                    else:
                        raise e
            # empty file is giving []
            if pin_assignments == []:
                pin_assignments = {}

            for pin in list(pin_assignments.keys()): # `pin` is a string!
                # comment?
                if pin == '#':
                    continue

                x = pin_assignments[pin]

                # comments?
                for p in list(x.keys()):
                    if p.startswith('#'):
                        del x[p]

                # FIXME: if existing PinSetup, merge the messages
                ps = PinSetup( pin,**x )
                if ps.setup():
                    changed = True
                    self.assignments[pin] = ps
                else:
                    debug(f"Skipped initial pin setup: {pin} : {topic_payload}")

        else:
            # Ensure file
            os.makedirs( os.path.dirname( pin_assignment_file ), exist_ok=True )
            with open( pin_assignment_file, 'w') as fh:
                fh.write('{}')
        debug(f"Pin assignments {self.assignments}")

        return changed

    async def argparse(self):
        # if you need to handle/look at any argparse
        await super().argparse()

        if self.parsed_args.list:
            with open(self.state_file(),'r') as fh:
                for l in fh:
                    sys.stdout.write(l)
            exit(0)

    def topic_for_add(self):
        return self.topic_for_command('add')
    def topic_for_remove(self):
        return self.topic_for_command('remove')
    def topic_for_list(self):
        return self.topic_for_get('pins')

    def topics(self):
        # FIXME: Getting called multiple times? cache it
        debug("Setup Topics")
        #debug(f"## pa {self.assignments}")
        pin_publishing = {}
        for p,defn in self.assignments.items():
            #debug( f"  ## {p} : {defn}")
            for m in defn.messages:
                topic = m['topic']
                payload = m['payload']
                #debug(f"    ## {p} : {m}")
                pin_publishing[ topic ] = { "description" : f"<{p}> -> {topic} = {payload}" }
        #debug(f"## pub {pin_publishing}")

        return super().topics( {
            # listen
            self.topic_for_get('+') : { 
                "handler" : self.handle_local_get_pins
            },
            self.topic_for_command('add/#') : {
                "handler" : self.handle_local_add_pin,
                "description" : "both: /add/<pin> payload,  /add/<pin>/<topic>"
            },
            self.topic_for_command('remove/#') : {
                "handler" : self.handle_local_remove_pin,
                "description" : "both: /remove/<pin> payload, /remove/<pin>/<topic>"
            },

            # publish:
            self.topic_for_event("<pin>"): { "description": "Boolean for the pin when it changes" },
            **pin_publishing,
        })

    # each handler
    async def handle_local_remove_pin(self, message):
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        # /remove/<pin> {payload} # all:True
        # /remove/<pin>/<topic> {payload}

        base_len = len(self.topic_for_get().split('/') )
        tail = str(message.topic).split('/')[base_len:] # -> add / <pin> / ...
        pin = tail[1]
        rest = tail[2:]
        debug(f"ADD pin={pin} rest={rest}")

        if not re.match(r'\d+$', pin ):
            debug(f"Expected add/<int>/..., but saw {message.topic}")
        pin = int(pin)

        if pin not in self.assignments.keys():
            debug(f"Remove, but that pin has no assignment {pin}")
            return
        
        pin_assignments = self.assignments[pin]  # [ {topic:$topic},... ]

        if  rest:
            # add/<pin>/topic { payload }
            topic = '/'.join(rest)
            payload = message.payload
            all = False
        else:
            # add/<pin> { topic:, payload: }
            if not isinstance(message.payload, dict):
                debug(f"Expected payload dictionary for {message.topic}, but saw {message.payload.__class__.__name__} {message.payload}")
                return

            topic = message.payload.get('topic','')
            payload = message.payload.get('payload','')
            all = message.payload.get('all',False)
        
        # we only handle topic w/payload assignments right now
        # (i.e. no recipes, etc)

        # same topic+payload or all:
        old_assignment = {"topic":topic, "payload":payload}
        for i in list(reversed(range(0, len(pin_assignments)))):
            topic_payload = pin_assignments[i]
        raise Exception("IMPLEMENT")
        self.assignments[pin] = [tp for tp in pin_assignments if not (all or old_assignment == tp) ]
        changed = [ tp for tp in pin_assignments if not tp in self.assignments[pin] ]
        if not self.assignments[pin]:
            self.assignments.pop(pin)
            if 'GPIO' in globals():
                GPIO.cleanup(pin)

        debug(f"Removed {pin} -> {changed}")
        if changed:
            self.save_state()

    async def handle_local_add_pin(self, message):
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        # /add/<pin> {payload}
        # /add/<pin>/<topic>

        base_len = len(self.topic_for_get().split('/') )
        tail = str(message.topic).split('/')[base_len:] # -> add / <pin> / ...
        pin = tail[1]
        rest = tail[2:]
        debug(f"ADD pin={pin} rest={rest}")

        if not re.match(r'\d+$', pin ):
            debug(f"Expected add/<int>/..., but saw {message.topic}")
        pin = int(pin)

        if  rest:
            # add/<pin>/topic { payload }
            topic = '/'.join(rest)
            payload = message.payload
        else:
            # add/<pin> { topic:, payload: }
            if not isinstance(message.payload, dict):
                debug(f"Expected payload dictionary for {message.topic}, but saw {message.payload.__class__.__name__} {message.payload}")
                return
            if len(excess:=set(message.payload.keys()) - set(['topic','payload']) ):
                debug(f"Expected only topic,payload keys, saw extra {excess} in {message.topic} -> {message.payload}")
                return 

            topic = message.payload.get('topic','')
            if not topic:
                debug(f"No topic in payload {message.topic} -> {message.payload}")
                return
            payload = message.payload.get('payload','')
        
        # we only handle topic w/payload assignments right now
        # (i.e. no recipes, etc)

        if self.setup_pin( pin, topic, payload):
            self.save_state()

    
    def state(self):
        d = { str(x.pin) : x.to_dict() for x in self.assignments.values() }
        debug(f"state {d}")
        return d

    async def handle_local_get_pins(self, message):
        """
        .../pins -> dict of pin:config
        .../all -> read and publish each pin
        .../$pinnumber -> that pin's config
        """
        # message.topic is a str, message.payload is json-decoded {...}, [...], or str (absence of payload is {})
        base_len = len(self.topic_for_get().split('/') )
        tail = str(message.topic).split('/')
        tail = tail[-1]
        #debug(f"TAIL {tail}")

        if tail == 'pins':
            payload = { str(pin):assignment for pin,assignment in self.assignments.items() }
            await self.mqtt_client.publish( self.topic_for_event('pins'), json.dumps( payload ) )
        elif tail == 'all':
            debug(f"## Read/pub all...")
            for assignment in self.assignments.values():
                debug(f"## Read/pub {assignment}...")
                await assignment.publish_on_event(self.mqtt_client, force=True)
        elif re.match(r'\d+$', tail ):
            pin = int(tail)
            actions = self.assignments.get(pin,[])
            await self.mqtt_client.publish( self.topic_for_event(str(pin)), json.dumps( actions ) )
        else:
            debug(f"Bad get/<pin>: {message.topic}")

    async def loop(self, first_time):

        async with asyncio.TaskGroup() as tg:
            for assignment in self.assignments.values():
                if assignment.keepalive:
                    debug(f"Has keepalive: [{assignment.pin}] {assignment.keepalive}")
                tg.create_task( assignment.publish_on_event(self.mqtt_client) )
                
        raise Exception("Should never get here")

    async def cleanup(self, e):
        await super().cleanup(e)
        if 'GPIO' in globals():
            for assignment in self.assignments.values():
                assignment.cleanup()
            GPIO.cleanup()

    def argparse_add_arguments(self, parser ):
        parser.add_argument('--list', help="list the saved button assignments, then exit", action='store_true')

# RUN
# note that Agent.run() does argparse for '-h|--help' in the base .run()
UIButtoner( ).async_run() # internally does asyncio.run( agent.run() )
